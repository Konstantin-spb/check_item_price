package ru.konstantin.check_item_price.test

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonObject
import org.jsoup.Jsoup
import ru.konstantin.check_item_price.shops.yandex_market.Product
import java.io.File

class JsonFromFileTest {
    val jsonParser = Json {
        ignoreUnknownKeys = true
    }
    val products = mutableSetOf<Product>()

    fun getJson() {
        val text = File("citylink_temp.html").readText()
        val doc = Jsoup.parse(text)
        val captchaElement = doc.select("script[src^=captcha]")
        if (captchaElement.isNotEmpty()) {
            println("Получили страницу с КАПТЧЕЙ...")
            return
        }

        val scriptList = doc.select("script[type=\"application/json\"]").filter { it.toString().contains("\"offer\":") }

        for (sc in scriptList) {
            val json = sc.toString().replace("</script>", "").substringAfter("\"offer\":")
                .substringBefore("\"offerShowPlace\":")
                .substringBeforeLast(",")
            try {
                val elems = jsonParser.parseToJsonElement(json).jsonObject
                for (elem in elems.entries) {
                    try {
                        val prod = jsonParser.decodeFromJsonElement<Product>(elem.value)
                        products.add(prod)
                        println("${prod.titles?.raw?:""}(${prod.productId}) = ${prod.price?.value}")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                if (doc.toString().contains("вперёд", true)) {
                    println("Вперёд...Есть дополнительные страницы.")
                }
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
        }
        println()
    }
}

fun main() {
    JsonFromFileTest().getJson()
}