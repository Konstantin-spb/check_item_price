package ru.konstantin.check_item_price

import kotlinx.coroutines.runBlocking
import ru.konstantin.check_item_price.shops.yandex_market.YandexMarket
import java.time.LocalDate


fun main() = runBlocking {
    while (true) {
        YandexMarket().run()
        println("Мы закончили парсить Яндекс Маркет...")
        println("Приступаем к еще одному повторению...")
    }
}