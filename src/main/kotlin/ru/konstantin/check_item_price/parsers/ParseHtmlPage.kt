package ru.konstantin.check_item_price.parsers

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonObject
import org.jsoup.Jsoup
//import ru.konstantin.check_item_price.network.DataFromResponse
import ru.konstantin.check_item_price.shops.yandex_market.Product
import ru.konstantin.check_item_price.shops.yandex_market.YandexWebEntity
import javax.sql.DataSource

class ParseHtmlPage {
   private val jsonParser = Json {
        ignoreUnknownKeys = true
    }

    fun getPageNumberFromLink(link: String): Int {
        val s = Regex(pattern = """&page=(\d+)""").find(input = link)?.groupValues?.get(1) ?: ""
        return if (s.isNotEmpty()) {
            s.toInt()
        } else {
            0
        }
    }

    fun checkNewPage(pageSource: String, yandexWebEntity: YandexWebEntity): String {
        if (pageSource.contains("Вперёд</a>", true)) {
            val pageNumber = getPageNumberFromLink(yandexWebEntity.link)
            if (pageNumber > 0) {
                return yandexWebEntity.link.replace("&page=\\d+".toRegex(), "&page=${pageNumber + 1}")
            } else {
                return "${yandexWebEntity.link}&page=2"
            }
        } else {
            return ""
        }
    }

    fun isHaveCaptcha(pageSource: String): Boolean {
        val doc = Jsoup.parse(pageSource)
        val captchaElement = doc.select("script[src^=captcha]")
        if (captchaElement.isNotEmpty()) {
//            println("Получили страницу с КАПТЧЕЙ...")
            return true
        }
        return false
    }

    fun getJsonWithProducts(sourcePage: String): String {
        val scriptList = Jsoup.parse(sourcePage).select("script[type=\"application/json\"]")
            .filter { it.toString().contains("\"offer\":") }

        for (sc in scriptList) {
            val json = sc.toString().replace("</script>", "").substringAfter("\"offer\":")
                .substringBefore("\"offerShowPlace\":")
                .substringBeforeLast(",")
            if (json.count() > 100) {
                return json
            }
        }
        return ""
    }

    fun getProductsFromJson(json: String): List<Product> {
        return jsonParser.parseToJsonElement(json).jsonObject.entries.map { jsonParser.decodeFromJsonElement<Product>(it.value) }
    }
}