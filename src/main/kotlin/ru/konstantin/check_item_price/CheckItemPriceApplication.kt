package ru.konstantin.check_item_price

import kotlinx.coroutines.*
import ru.konstantin.check_item_price.shops.beruru.BeruDotRu
import ru.konstantin.check_item_price.shops.citilink.Citilink
import ru.konstantin.check_item_price.shops.eldorado.Eldorado
import ru.konstantin.check_item_price.shops.mvideo.Mvideo

//class CheckItemPriceApplication

//@InternalAPI
suspend fun main() = runBlocking {
    val beruDotRu = true
    val citilink = true
    val eldorado = true
    val mvideo = true
    val allJobs = mutableListOf<Job>()

//    val scope = CoroutineScope(Dispatchers.Main)
    val scope = CoroutineScope(Job() + Dispatchers.Default)

    if (beruDotRu) {
        val jobBeruDotRu = scope.launch {
//            withContext(coroutineContext) {
                BeruDotRu().start("Санкт-Петербург")
//            }
        }
        allJobs.add(jobBeruDotRu)
    }

////                =====================================Citilink_start
    if (citilink) {
        val jobCitilink = scope.launch {
//            withContext(coroutineContext) {
    //                val cookieSpace = "spb_cl"
                Citilink().start("Санкт-Петербург")
    //    citilink.start("Москва", "msk_cl")
    //    citilink.start("Екатеринбург", "ekat_cl")}
//            }
        }
        allJobs.add(jobCitilink)
    }
////////=====================================Citilink_end


//////=====================================Eldorado_start
    if (eldorado) {
        val jobEldorado = scope.launch() {
//            withContext(coroutineContext) {
                Eldorado().start("Санкт-Петербург")
//            }
        }
        allJobs.add(jobEldorado)
    }
////    =====================================Eldorado_end
    if (mvideo) {
        val jobMvideo = scope.launch {
            Mvideo().start("Санкт-Петербург")
        }
        allJobs.add(jobMvideo)
    }

//    allJobs.forEach { it.cancelAndJoin() }
    allJobs.joinAll()
    scope.coroutineContext.cancelChildren()
//    while (true) {
//        if (allJobs[0].isActive) {
//            println("isActive}")
//        }
//        if (allJobs[0].isCompleted) {
//            println("isCompleted}")
//            delay(5000)
//            break
//        }
//        if (allJobs[0].isCancelled) {
//            println("isCancelled}")
//        }
//        delay(1000)
//    }
}

//    val runtime = Runtime.getRuntime()
//    val proc = runtime.exec("shutdown -s -t 0")
//    exitProcess(0)
