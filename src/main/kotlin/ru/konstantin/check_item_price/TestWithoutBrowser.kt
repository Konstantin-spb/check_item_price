package ru.konstantin.check_item_price

import kotlinx.serialization.json.jsonObject
import ru.konstantin.check_item_price.network.SendRequest
import ru.konstantin.check_item_price.parsers.ParseHtmlPage
import ru.konstantin.check_item_price.shops.Item
import ru.konstantin.check_item_price.shops.yandex_market.Product
import ru.konstantin.check_item_price.shops.yandex_market.YandexMarket
import ru.konstantin.check_item_price.shops.yandex_market.YandexWebEntity

class TestWithoutBrowser {
    private var allTypeLinks = mutableSetOf<YandexWebEntity>()

    private fun markAsUsed(yandexWebEntity: YandexWebEntity) {
        allTypeLinks.remove(yandexWebEntity)
        yandexWebEntity.alreadyUsed = true
        allTypeLinks.add(yandexWebEntity)
    }

    fun run() {
        var pageCount = 0
        val allLinks = YandexMarket().getProxyListFromFile("linksToCategory.txt")
            .map { YandexWebEntity(link = "$it&cpa=1&onstock=0", alreadyUsed = false) }

        allTypeLinks.addAll(allLinks.map { YandexWebEntity("${it.link}") })
//        allTypeLinks.sortedByDescending { it.link }
        val parseHtmlPage = ParseHtmlPage()
        val sendRequest = SendRequest()
        val items = mutableListOf<Item>()
        val itemCountList = mutableSetOf<String>()

        while (true) {
            if (allTypeLinks.filter { !it.alreadyUsed }.count() <= 0) {
                break
            }
            try {
                val yandexWebEntity = allTypeLinks.filter { !it.alreadyUsed }[0]
                val pageSource = sendRequest.sendGetRequestToYandex(yandexWebEntity.link)
                pageCount++
                println(yandexWebEntity.link)

                val jsonString = parseHtmlPage.getJsonWithProducts(pageSource)
                val products = try {
                    parseHtmlPage.getProductsFromJson(jsonString)
                } catch (e: Exception) {
//                    e.printStackTrace()
                    listOf<Product>()
                }

                for (it in products) {
                    try {
                        val newItem = Item()
                        newItem.price = it.price?.value?.times(100)
//                newItem.productId = it.select("h3 > a").attr("href").substringBefore("?").substringAfterLast("/")
                        newItem.productId = it.productId.toString()
                        newItem.productName = it.titles?.raw?:""
                        newItem.shopId = 5
                        newItem.cityName = "Санкт-Петербург"
                        newItem.linkToItem = it.slug?:""
                        items.add(newItem)
//                println(newItem)
                    } catch (e: Exception) {
//                e.printStackTrace()
                    }
                    itemCountList.addAll(items.map { it.productId })
                }

//                val items = getItemsFromSourcePage(pageSource, 5, "Санкт-Петербург")
//                itemCount += items.count()
//                println(
//                    "Всего $itemCount товаров. Осталось необработанных ссылок ${
//                        allTypeLinks.filter { !it.alreadyUsed }.count()
//                    }."
//                )
//                if (items.count() > 0) {
//                    sendItemsToServer(items)
//                    //Добавляем ссылку в коллекцию, если есть следующая страница
//                    addNewPageLinkToCollectionIfNeed(pageSource, yandexWebEntity)
//                    itemCountList.addAll(items.map { it.productId })
//                }
//                //Присваиваем ссылке true, так как ее уже использовали.
                markAsUsed(yandexWebEntity)
                println("=============================Уникальных товаров ${itemCountList.count()}======обработано $pageCount страниц.======================================================================")
            } catch (e: Exception) {
//                webDriver.quit()
////                //Если ошибка, то есть возможность пересоздать drive для небольшого ускорения
//                webDriver = createWebDriver(isIncognito = true, userAgent = getUserAgentFromFile(), proxy)
//                e.printStackTrace()
//                println("Создадим новый WebDriver")
            }
        }
        println()
    }
}

fun main() {
    TestWithoutBrowser().run()
}