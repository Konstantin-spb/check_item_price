package ru.konstantin.check_item_price.network
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GimmeProxy(
    @SerialName("anonymityLevel")
    var anonymityLevel: Int? = null,
    @SerialName("cookies")
    var cookies: Boolean? = null,
    @SerialName("country")
    var country: String? = null,
    @SerialName("curl")
    var curl: String? = null,
    @SerialName("get")
    var `get`: Boolean? = null,
    @SerialName("ip")
    var ip: String? = null,
    @SerialName("ipPort")
    var ipPort: String? = null,
    @SerialName("otherProtocols")
    var otherProtocols: OtherProtocols? = null,
    @SerialName("port")
    var port: String? = null,
    @SerialName("post")
    var post: Boolean? = null,
    @SerialName("protocol")
    var protocol: String? = null,
    @SerialName("referer")
    var referer: Boolean? = null,
    @SerialName("speed")
    var speed: Double? = null,
    @SerialName("supportsHttps")
    var supportsHttps: Boolean? = null,
    @SerialName("tsChecked")
    var tsChecked: Int? = null,
    @SerialName("type")
    var type: String? = null,
    @SerialName("unixTimestamp")
    var unixTimestamp: Int? = null,
    @SerialName("unixTimestampMs")
    var unixTimestampMs: Long? = null,
    @SerialName("user-agent")
    var userAgent: Boolean? = null,
    @SerialName("verifiedSecondsAgo")
    var verifiedSecondsAgo: Int? = null,
    @SerialName("websites")
    var websites: Websites? = null
) {
    @Serializable
    class OtherProtocols(
    )

    @Serializable
    data class Websites(
        @SerialName("amazon")
        var amazon: Boolean? = null,
        @SerialName("example")
        var example: Boolean? = null,
        @SerialName("google")
        var google: Boolean? = null,
        @SerialName("google_maps")
        var googleMaps: Boolean? = null,
        @SerialName("yelp")
        var yelp: Boolean? = null
    )
}