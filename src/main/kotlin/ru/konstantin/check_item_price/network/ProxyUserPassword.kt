package ru.konstantin.check_item_price.network

data class ProxyUserPassword(val proxyString: String) {
    var scheme: String = ""
    var host: String = ""
    var port: String = ""
    var proxyUser: String = ""
    var proxyPassword: String = ""

    init {
        if (proxyString.contains("@")) {
            val partsOfString = proxyString.split("@")
            proxyUser = partsOfString[0].substringBefore(":")
            proxyPassword = partsOfString[0].substringAfter(":")

            if (proxyString.isNotBlank()) {
                if (proxyString.contains("://")) {
                    scheme = proxyString.substringBefore("://")
                }
                host = partsOfString[1].substringBeforeLast(":").substringAfter("://")
                port = partsOfString[1].substringAfterLast(":")
            }
        } else {
            host = proxyString.substringBefore(":")
            port = proxyString.substringAfter(":")
        }
//        nfxVZvKQ:kVahzhY1@91.239.85.47:57924
    }

    override fun toString(): String {
        val sb = StringBuffer()
        sb.append("$proxyUser:$proxyPassword@")
        if(scheme.isNotEmpty()){
            sb.append("$scheme://")
        }
        sb.append("$host:$port")
        return sb.toString()
    }
}