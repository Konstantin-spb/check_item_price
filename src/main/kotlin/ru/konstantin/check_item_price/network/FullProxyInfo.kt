
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FullProxyInfo(
    @SerialName("asn")
    var asn: String? = null,
    @SerialName("city")
    var city: String? = null,
    @SerialName("connectionType")
    var connectionType: String? = null,
    @SerialName("cookies")
    var cookies: Boolean? = null,
    @SerialName("country")
    var country: String? = null,
    @SerialName("get")
    var `get`: Boolean? = null,
    @SerialName("ip")
    var ip: String? = null,
    @SerialName("isp")
    var isp: String? = null,
    @SerialName("lastChecked")
    var lastChecked: Int? = null,
    @SerialName("port")
    var port: String? = null,
    @SerialName("post")
    var post: Boolean? = null,
    @SerialName("proxy")
    var proxy: String? = null,
    @SerialName("randomUserAgent")
    var randomUserAgent: String? = null,
    @SerialName("referer")
    var referer: Boolean? = null,
    @SerialName("requestsRemaining")
    var requestsRemaining: Int? = null,
    @SerialName("state")
    var state: String? = null,
    @SerialName("type")
    var type: String? = null,
    @SerialName("userAgent")
    var userAgent: Boolean? = null
) {
    override fun toString(): String {
        return "FullProxyInfo(asn=$asn, city=$city, connectionType=$connectionType, cookies=$cookies, country=$country, `get`=$`get`, ip=$ip, isp=$isp, lastChecked=$lastChecked, port=$port, post=$post, proxy=$proxy, randomUserAgent=$randomUserAgent, referer=$referer, requestsRemaining=$requestsRemaining, state=$state, type=$type, userAgent=$userAgent)"
    }
}