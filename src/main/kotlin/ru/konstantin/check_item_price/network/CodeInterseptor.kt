package ru.konstantin.check_item_price.network

import okhttp3.Interceptor
import okhttp3.Response
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.io.IOException


internal class CodeInterseptor : Interceptor {
    val logger: Logger = LogManager.getLogger(CodeInterseptor::class.java)
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val authenticationRequest = originalRequest.newBuilder()
                .header("TestHeader", "fail")
                .build()
        val origResponse = chain.proceed(authenticationRequest)
        logger.info(origResponse.code)


        // server should give us a 403, since the header contains 'fail'
        return if (origResponse.code == 403) {
            val refreshToken = "abcd" // you got this from Auth0 when logging in

            // start a new synchronous network call to Auth0
//            val newIdToken: String = fetchNewIdTokenFromAuth0(refreshToken)

            // make a new request with the new id token
            val newAuthenticationRequest = originalRequest.newBuilder()
                    .header("TestHeader", "succeed")
                    .build()

            // try again

            // hopefully we now have a status of 200
            chain.proceed(newAuthenticationRequest)
        } else {
            origResponse
        }
    }
}