package ru.konstantin.check_item_price.network

import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.extensions.jsonBody
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.apache.http.client.CircularRedirectException
import org.apache.logging.log4j.LogManager
import ru.konstantin.check_item_price.parsers.ParseHtmlPage
import ru.konstantin.check_item_price.shops.yandex_market.YandexWebEntity
import java.io.File
import java.net.CookieManager
import java.net.CookiePolicy
import java.net.InetSocketAddress
import java.net.Proxy
import java.util.*
import java.util.concurrent.TimeUnit
import okhttp3.CookieJar

//Сначала обязательно создать okHttpClient
class SendRequest() {
    val logger = LogManager.getLogger(SendRequest::class.java)
    var lastCookieString = ""
//    var serializableCookieJar: SerializableCookieJar = SerializableCookieJar(this)

    val proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress("node-ru-57.astroproxy.com", 10716))
    val httpClient = OkHttpClient.Builder()
        .connectTimeout(5, TimeUnit.SECONDS)
        .writeTimeout(5, TimeUnit.SECONDS)
        .readTimeout(5, TimeUnit.SECONDS)
        .callTimeout(10, TimeUnit.SECONDS)
//        .cookieJar(serializableCookieJar)
        .followRedirects(true)
        .followSslRedirects(true)
        .proxy(proxy)
        .proxyAuthenticator { _, response ->
            val credential = Credentials.basic("konstantin", "Handbook232323")
            response.request.newBuilder()
                .header("Proxy-Authorization", credential)
                .build()
        }
        .build()
    //    val userAgent = UserAgent() //create new userAgent (headless browser).
    private var allTypeLinks = mutableSetOf<YandexWebEntity>()

    lateinit var okHttpClient: OkHttpClient

//    fun getRequestByJaunt(url: String): DataFromResponse {
//        userAgent.visit(url)
//        println(userAgent.response.headers)
//        println(userAgent.response.status)
//        println(userAgent.doc.url)
//        return DataFromResponse(
//            if (userAgent.doc.url.contains("showcaptcha?")) {
//                302
//                  } else {
//                userAgent.response.status
//            },
//            userAgent.response.headers,
//            userAgent.doc.innerHTML())
//    }

    fun createOkHttpClient(proxyUserPassword: ProxyUserPassword): OkHttpClient {
//        val cookieJar: CookieJar = PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(context))
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        val httpClientBuilder = OkHttpClient.Builder()
//                .cookieJar(JavaNetCookieJar(cookieManager))
            .connectTimeout(5, TimeUnit.SECONDS)
            .writeTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .callTimeout(10, TimeUnit.SECONDS)
            .followRedirects(false)
            .followSslRedirects(false)
//        if (proxyUserPassword.host.isNotEmpty()) {
//            httpClientBuilder.proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress(proxyUserPassword.host, proxyUserPassword.port.toInt())))
//            if (proxyUserPassword.proxyUser.isNotEmpty() && proxyUserPassword.proxyPassword.isNotEmpty()) {
//                httpClientBuilder.proxyAuthenticator { _, response ->
//                    val credential = Credentials.basic(proxyUserPassword.proxyUser, proxyUserPassword.proxyPassword)
//                    response.request.newBuilder()
//                        .header("Proxy-Authorization", credential)
//                        .build()
//                }
//            }
//        }
        return httpClientBuilder.build()
    }

//    fun sendGetRequest(url: String,
//                       useragent: String = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 YaBrowser/20.11.3.183 Yowser/2.5 Safari/537.36"): DataFromResponse {
//        val request = Request.Builder()
//            .get()
////            .url("https://pokupki.market.yandex.ru/catalog/smartfony-i-aksessuary/54437?hid=91461")
//            .header("User-Agent", useragent)
//            .url(url)
//            .build()
//
//        val response = this.okHttpClient.newCall(request)
//            .execute()
//
//        println("----------------------------------------")
//        println("${response.code} - ${response.message}")
//        println(response.toString())
//        for (head in response.headers) {
//            println(head)
//        }
//        val dataFromResponse = DataFromResponse(response.code, MultiMap<String, String>(), response.body?.string()?:"")
//        response.close()
//        return dataFromResponse
//    }

    fun sendGetRequestToYandex(link: String): String {
//        val proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress("node-ru-81.astroproxy.com", 11039))



//        "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 Edg/87.0.664.75"
        val request = Request.Builder()
            .get()
            .header("Connection", "keep-alive")
            .header("Pragma", "no-cache")
            .header("Cache-Control", "no-cache")
            .header("Upgrade-Insecure-Requests", "1")
            .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
            .header("Sec-Fetch-Site", "none")
            .header("Sec-Fetch-Mode", "navigate")
            .header("Sec-Fetch-Dest", "document")
            .header("Accept-Language", "ru,en;q=0.9,en-GB;q=0.8,en-US;q=0.7")
//            .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 Edg/87.0.664.75")
            .header("User-Agent", getUserAgentFromFile())
//            .url("https://pokupki.market.yandex.ru/catalog/smartfony-i-aksessuary/54437?hid=91461")
            .url(link)
            .build()

        val response = httpClient.newCall(request)
            .execute()

        println("----------------------------------------")
        println("${response.code} - ${response.message}")

        var pageSource = ""
        if (response.code == 200) {
            pageSource = response.body?.string() ?: ""
            if (ParseHtmlPage().isHaveCaptcha(pageSource)) {
                println("На странице найдена КАПТЧА...1")
//                readLine()
//                    continue
            } else {
//                println(pageSource)
                if (pageSource.contains("Вперёд</a>", true)) {
                    response.isSuccessful
                }
            }
        }

        response.close()
//        println(pa)
//            println(response.body?.string())
//        }
        return pageSource
    }

    fun getUserAgentFromFile(): String {
        return File("useragentlist.txt").readLines().shuffled().first()
    }

    private fun addNewPageLinkToCollectionIfNeed(pageSource: String, yandexWebEntity: YandexWebEntity) {
        val parseHtmlPage = ParseHtmlPage()
        val srt = parseHtmlPage.checkNewPage(pageSource, yandexWebEntity)
        if (srt.isNotEmpty()) {
            //Если есть кнопка 'далее', то делаем ссылку на следующую страницу и если ее нет в коллекци - помещаем в коллекцию.
            if (allTypeLinks.filter { it.link == srt }
                    .count() <= 0) {
                allTypeLinks.add(YandexWebEntity(srt))
            }
        }
    }

    fun sendJsonPost(jsonString: String): String {
        var responseString = ""
//        val restUrl = "http://cashstats.com/check_price_rest_sb/additems"
        val restUrl = "http://173.214.244.70/check_price_rest_sb/additems"
//        Fuel.post(restUrl)
        FuelManager().post(restUrl)
            .jsonBody(jsonString)
//                .also { println(it) }
            .response { _, response, result ->
                //                    println(request)
//                    if (response.statusCode != 200) {
                responseString = response.toString()
//                    }
                val (bytes, _) = result
                if (bytes != null) {
                    logger.info("${response.statusCode} => [response bytes] ${String(bytes)}")
                }
            }
        return responseString
    }

//    suspend fun apacheHttpClientRequest(url: String, headers: Map<String, String> = mapOf(), method: String = "get", proxy: String = "", payloadData: String = "", timeout: Int = 30): MutableMap<String, String> {
//        val sslcontext: SSLContext = SSLContexts.custom().loadTrustMaterial(null, object : TrustStrategy {
//            @Throws(CertificateException::class)
//            override fun isTrusted(arg0: Array<X509Certificate?>?, arg1: String?): Boolean {
//                return true
//            }
//        }).build()
//
//        // Allow TLSv1 protocol only
//        val sslsf = SSLConnectionSocketFactory(
//                sslcontext, arrayOf("TLSv1"),
//                null,
//                SSLConnectionSocketFactory.getDefaultHostnameVerifier()
//        )
//        val httpclient = HttpClients.custom()
////                .setDefaultCredentialsProvider(credsProvider)
//                .setSSLSocketFactory(sslsf)
//                .setDefaultRequestConfig(RequestConfig.custom()
//                        .setCookieSpec(CookieSpecs.STANDARD).build())
//                .build()
//
//        var pageSource: String = ""
//        var response: CloseableHttpResponse? = null
//        val answer = mutableMapOf<String, String>()
//        try { // Create AuthCache instance
//            val requestConfig = RequestConfig.custom();
//            requestConfig.setConnectTimeout(timeout * 1000)
//            requestConfig.setConnectionRequestTimeout(timeout * 1000)
//            requestConfig.setSocketTimeout(timeout * 1000)
//            requestConfig.setCookieSpec(CookieSpecs.STANDARD)
//            if (proxy.isNotEmpty()) {
//                val ip = proxy.substringBeforeLast(":").substringAfter("://")
//                val port = proxy.substringAfterLast(":").toInt()
//                val scheme = proxy.substringBefore("://")
//                requestConfig.setProxy(HttpHost(ip, port, scheme))
////                requestConfig.setProxy(HttpHost(proxy))
//            }
//
//            //Если запрос GET, то используем этот запрос
//            val httpGet = HttpGet(url)
//            //Если запрос POST, то используем этот запрос
//            val httppost = HttpPost(url)
//
//            if (method.equals("get", true)) {
////                logger.info("----------------------------------------")
////                logger.info("Executing request " + httpGet.requestLine)
//                httpGet.config = requestConfig.build()
//                for (h in headers.entries) {
//                    httpGet.setHeader(h.key, h.value)
//                }
//                try {
//                    withTimeoutOrNull(15000) {
//                        response = httpclient.execute(httpGet)
//                    }
//                } catch (e: CircularRedirectException) {
//                    answer.put("404", "404")
//                    return answer
//                } catch (e: Exception) {
//                    logger.info(e.message)
////                    response = httpclient.execute(httpGet)
//                }
//            } else if (method.equals("post", true)) {
////                logger.info("----------------------------------------")
////                logger.info("Executing request " + httppost.requestLine)
//                val se = StringEntity(payloadData);
//                httppost.entity = se
//                httppost.config = requestConfig.build()
//                for (h in headers.entries) {
//                    httppost.setHeader(h.key, h.value)
//                }
//                withTimeoutOrNull(15000) {
//                    response = httpclient.execute(httppost)
//                }
//            }
//
//            try {
//                val entity = response?.entity
////                logger.info(response?.statusLine)
//                pageSource = EntityUtils.toString(entity)
//                if (response?.statusLine?.statusCode == 404) {
//                    pageSource = "404"//чтобы не выбирадся новый прокси, передаем страницу не содержащую ошибки, но и без товара
//                } else if (response?.statusLine?.statusCode == 456) {
//                    pageSource = ""//требуется повторный запрос, прокси менять не надо
//                }
//                answer.put(response?.statusLine?.statusCode.toString(), pageSource)
//            } catch (e: Exception) {
//                logger.info(e.message)
//            } finally {
//                response?.close()
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        } finally {
//            httpclient.close()
//        }
//
//        return answer
//    }

    suspend fun okHttpClientRequest(
        url: String,
        headers: Map<String, String> = mapOf(),
        method: String = "get",
        proxy: String = "",
        payloadData: String = "",
        timeout: Long = 15,
        baseUrl: String = ""
    ): MutableMap<String, String> {
        var statusCode = 0
//        var headersSetCookie = ""
        var location = url

//        val sslcontext: SSLContext = SSLContexts.custom().loadTrustMaterial(null, object : TrustStrategy {
//            @Throws(CertificateException::class)
//            override fun isTrusted(arg0: Array<X509Certificate?>?, arg1: String?): Boolean {
//                return true
//            }
//        }).build()

        // Allow TLSv1 protocol only
//        val sslsf = SSLConnectionSocketFactory(
//                sslcontext, arrayOf("TLSv1"),
//                null,
//                SSLConnectionSocketFactory.getDefaultHostnameVerifier()
//        )

        val clientBuilder = OkHttpClient.Builder()
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .callTimeout(timeout, TimeUnit.SECONDS)
            .followRedirects(false)
            .followSslRedirects(false)
            .connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.COMPATIBLE_TLS))
//                .sslSocketFactory(sslcontext)
//                .addInterceptor(CodeInterseptor())

        val requestBuilder = Request.Builder()

        for (header in headers.entries) {
            requestBuilder.addHeader(header.key, header.value)
        }

        var pageSource: String = ""
        val answer = mutableMapOf<String, String>()
        while (true) {
            try {
                if (proxy.isNotBlank()) {
                    val ip = proxy.substringBeforeLast(":").substringAfter("://")
                    val port = proxy.substringAfterLast(":").toInt()
                    val p: Proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress(ip, port))
                    clientBuilder.proxy(p)
                }

                if (method.equals("post", true)) {
                    val body = payloadData.toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
                    requestBuilder.post(body)
                }

                try {
                    requestBuilder.url(location)
                    clientBuilder.build().newCall(requestBuilder.build()).execute().use { response ->
                        pageSource = response.body?.string() ?: ""
                        statusCode = response.code
                        lastCookieString = response.headers("Set-Cookie").joinToString(";")
                        location = response.header("location") ?: location
                        if (!location.contains("https://") && !location.contains("http://")) {
                            location = baseUrl + response.header("location")
                        }
//                        logger.info(headersSetCookie)
                    }
                    if (statusCode == 404) {
                        pageSource =
                            "404"//чтобы не выбирался новый прокси, передаем страницу не содержащую ошибки, но и без товара
                    } else if (statusCode == 456) {
                        pageSource = ""//требуется повторный запрос, прокси менять не надо
                    } else if (statusCode in 300..399) {//требуется редирект, передаем куки из ответа в новый запрос
                        requestBuilder.header("cookie", lastCookieString)
//                        logger.info(location)
//                        logger.info(headersSetCookie)
                        continue
                    }

                    answer[statusCode.toString()] = pageSource
                    break
                } catch (e: CircularRedirectException) {
                    answer["404"] = "404"
                    return answer
                } catch (e: Exception) {
                    logger.info(e.message)
                    e.printStackTrace()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        return answer
    }

//    fun InputStream.readTextAndClose(charset: Charset = Charsets.UTF_8): String {
//        return this.bufferedReader(charset).use { it.readText() }
//    }

//    fun okHttpClientNew(url: String, proxyUserPassword: ProxyUserPassword): Response {
//        val httpClientBuilder = OkHttpClient.Builder()
//            .connectTimeout(5, TimeUnit.SECONDS)
//            .writeTimeout(5, TimeUnit.SECONDS)
//            .readTimeout(5, TimeUnit.SECONDS)
//            .callTimeout(10, TimeUnit.SECONDS)
//            .followRedirects(false)
//            .followSslRedirects(false)
//        if (proxyUserPassword != null && proxyUserPassword.host.isNotEmpty()) {
//            httpClientBuilder.proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress(proxyUserPassword.host, proxyUserPassword.port.toInt())))
//            if (proxyUserPassword.proxyUser.isNotEmpty() && proxyUserPassword.proxyPassword.isNotEmpty()) {
//                httpClientBuilder.proxyAuthenticator { _, response ->
//                    val credential = Credentials.basic("nfxVZvKQ", "kVahzhY1")
//                    response.request.newBuilder()
//                        .header("Proxy-Authorization", credential)
//                        .build()
//                }
//            }
//        }
//
//        val httpClient = httpClientBuilder.build()
//
//        val request = Request.Builder()
//            .get()
//            .url(url)
//            .build()
//
//        val response = httpClient.newCall(request)
//            .execute()
//
//        println("----------------------------------------")
//        println("${response.code} - ${response.message}")
//
//        println(response.toString())
//        response.close()
//        return response
//    }
}