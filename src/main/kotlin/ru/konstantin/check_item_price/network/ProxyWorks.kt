package ru.konstantin.check_item_price.network

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.apache.logging.log4j.LogManager
import org.jsoup.Jsoup
import java.io.File


class ProxyWorks {
    val logger = LogManager.getLogger(ProxyWorks::class.java)
    fun getProxyFromFile(): Set<String> {
//        val configFile = File(filepPath)
//        if (!configFile.exists()) {
//            logger.info("config.txt not exist, the program has stopped work")
//            return setOf()
//        }
//        return File("C:\\Users\\konstantin\\IdeaProjects\\check_item_price\\utils\\ProxyChecker\\Out\\proxies.txt").readLines().toSet()
        return File("C:\\Users\\User\\IdeaProjects\\check_item_price\\proxies.txt").readLines().toSet()
    }

//    fun getProxyListFromInet(): Set<String> {
//        val webClient = WebClient(BrowserVersion.BEST_SUPPORTED)
//        webClient.options.isJavaScriptEnabled = true;
//        webClient.options.isThrowExceptionOnScriptError = false;
//        webClient.options.isThrowExceptionOnFailingStatusCode = false;
//        val linkList = mutableSetOf<String>()
//
//        webClient.use { wClient ->
//            withTimeoutOrNull(60000) {
//                for (count in 1..10) {
//                    val page: HtmlPage = wClient.getPage("https://premproxy.com/list/type-0$count.htm")
////                val page: HtmlPage = wClient.getPage("http://free-proxy.cz/en/proxylist/country/all/http/ping/all/5/$count")
////                val elems = page.getByXPath<HtmlElement>("//table[@id='proxy_list']/tbody/tr")
//                    try {
//                        val elems = page.getByXPath<HtmlElement>("//*[@id='proxylistt']/tbody/tr")
//                        for (elem in elems) {
//                            try {
//                                val ipAndPort = elem.getElementsByTagName("td")[0].asText().replace("[a-zA-Z]".toRegex(), "")
//                                //                        val ip = elem.getElementsByTagName("td")[0].asText()
//                                //                        val port = elem.getElementsByTagName("td")[1].asText()
//                                //                        val port = elem.getElementsByTagName("td")[1].asText()
//                                //                        val scheme = elem.getElementsByTagName("td")[2].asText()
//                                //                        val scheme = elem.getElementsByTagName("td")[2].asText()
//                                //                        linkList.add("${scheme.toLowerCase()}://$ip:$port")
//                                linkList.add("http://$ipAndPort")
//                            } catch (e: Exception) {
//                                //                    e.printStackTrace()
//                            }
//                        }
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    } finally {
//                        logger.info("Quantity of proxies is ${linkList.count()}")
////                        delay(5000)
//                    }
//                }
//            }
//        }
//
////        val configFile = File(filepPath)
////        if (!configFile.exists()) {
////            logger.info("config.txt not exist, the program has stopped work")
////            return setOf()
////        }
////        return File(filepPath).readLines().toSet()
//        return linkList
//    }

    fun checkProxyJsoup(proxySource: String): String {
        var trueProxy = ""
        val ip = proxySource.substringBeforeLast(":").substringAfter("://")
        val port = proxySource.substringAfterLast(":").toInt()
        val connection = Jsoup.connect("https://www.citilink.ru/")
            .proxy(ip, port)
            .timeout(5000)
//        val response = SendingRequest().apacheHttpClientRequest("https://citilink.ru/", proxy = proxySource, timeout = 3)
//        val response = SendRequest().okHttpClientRequest("https://citilink.ru/", proxy = proxySource, timeout = 7)
//        val response = connection.execute()
//        response.statusCode()

        val response = connection.execute()
        //Printing the status line
        if (response.statusCode() == 200) {
//             -> {
//                trueProxy = "${proxySource.substringBeforeLast(":").substringAfter("://")}:${proxySource.substringAfterLast(":")}"
            trueProxy = proxySource
            logger.info("True work proxy is $proxySource")
//            }
        }

        return trueProxy
    }

    fun getTrueProxy(useProxy: Boolean = true): String {
        var trueProxy = ""

        if (useProxy) {
            val pw = ProxyWorks()
            while (true) {
                try {
//                    for (pa in pw.getProxyListInet().shuffled().toSet()) {
                    for (pa in pw.getProxyFromFile().toSet()) {
                        try {
                            trueProxy = pw.checkProxyJsoup(pa)
                            if (trueProxy.isNotEmpty()) {
                                logger.info(trueProxy)
                                break
                            }
                        } catch (e: Exception) {
                            logger.info("Proxy do not work\t$pa")
                            e.message
                        }
                    }
                    if (trueProxy.isNotEmpty()) {
                        break
                    }
                } catch (e: Exception) {
                    continue
                }
            }
//            proxyIp = trueProxy.substringBefore(":")
//            proxyPort = trueProxy.substringAfter(":").toInt()
        }
        return trueProxy
    }

    fun getGimmeProxy() {
        val doc = Jsoup.connect("https://gimmeproxy.com/api/getProxy")
            .ignoreContentType(true)
            .get()

        val str = doc.body().text()
        val gimmeProxy = Json { ignoreUnknownKeys = true }.decodeFromString<GimmeProxy>(str)
        File("proxies.txt").appendText("${gimmeProxy.ipPort}\n")
        println(gimmeProxy.ipPort)
    }

    fun getGettProxyList() {
        val doc = Jsoup.connect("https://api.getproxylist.com/proxy")
            .ignoreContentType(true)
            .get()
//            .header("cookie", "__cfduid=dbc6184472d3133a1ea584b5b47f39f731607026588; _ga=GA1.2.846942194.1607026590; _gid=GA1.2.400097842.1607026590; __stripe_mid=5be8d774-5f7c-42bf-9990-b8b75b538a075ba935; __stripe_sid=e9a19645-3f78-49ce-b13f-0fd7ab8e7bf1ad7490")

        val str = doc.body().text()
        val gettProxyList = Json { ignoreUnknownKeys = true }.decodeFromString<GettProxyList>(str)
        File("proxies.txt").appendText("${gettProxyList.ip}:${gettProxyList.port}\n")
        println("${gettProxyList.ip}:${gettProxyList.port}")
    }
}