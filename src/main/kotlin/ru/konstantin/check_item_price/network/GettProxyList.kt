package ru.konstantin.check_item_price.network


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GettProxyList(
    @SerialName("allowsCookies")
    var allowsCookies: Boolean? = null,
    @SerialName("allowsCustomHeaders")
    var allowsCustomHeaders: Boolean? = null,
    @SerialName("allowsHttps")
    var allowsHttps: Boolean? = null,
    @SerialName("allowsPost")
    var allowsPost: Boolean? = null,
    @SerialName("allowsRefererHeader")
    var allowsRefererHeader: Boolean? = null,
    @SerialName("allowsUserAgentHeader")
    var allowsUserAgentHeader: Boolean? = null,
    @SerialName("anonymity")
    var anonymity: String? = null,
    @SerialName("cache")
    var cache: Cache? = null,
    @SerialName("connectTime")
    var connectTime: String? = null,
    @SerialName("country")
    var country: String? = null,
    @SerialName("downloadSpeed")
    var downloadSpeed: String? = null,
    @SerialName("ip")
    var ip: String? = null,
    @SerialName("lastTested")
    var lastTested: String? = null,
    @SerialName("_links")
    var links: Links? = null,
    @SerialName("port")
    var port: Int? = null,
    @SerialName("protocol")
    var protocol: String? = null,
    @SerialName("secondsToFirstByte")
    var secondsToFirstByte: String? = null,
    @SerialName("stats")
    var stats: Stats? = null,
    @SerialName("uptime")
    var uptime: String? = null
) {
    @Serializable
    data class Cache(
        @SerialName("hit")
        var hit: String? = null,
        @SerialName("key")
        var key: String? = null
    )

    @Serializable
    data class Links(
        @SerialName("_parent")
        var parent: String? = null,
        @SerialName("_self")
        var self: String? = null
    )

    @Serializable
    data class Stats(
        @SerialName("count")
        var count: String? = null
    )
}