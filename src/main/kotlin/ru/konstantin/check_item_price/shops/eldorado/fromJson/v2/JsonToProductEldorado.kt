package ru.konstantin.check_item_price.shops.eldorado.fromJson.v2

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class JsonToProductEldorado(
    @JsonProperty("brandCode")
    var brandCode: String?,
    @JsonProperty("brandId")
    var brandId: Int?,
    @JsonProperty("brandName")
    var brandName: String?,
    @JsonProperty("categoryId")
    var categoryId: Int?,
    @JsonProperty("categoryName")
    var categoryName: String?,
    @JsonProperty("classificationCategoryId")
    var classificationCategoryId: Int?,
    @JsonProperty("code")
    var code: String?,
    @JsonProperty("dateCreate")
    var dateCreate: String?,
    @JsonProperty("deliveryAvailable")
    var deliveryAvailable: Boolean?,
    @JsonProperty("digitalDelivery")
    var digitalDelivery: Boolean?,
    @JsonProperty("fastOrder")
    var fastOrder: Boolean?,
    @JsonProperty("hideBuy")
    var hideBuy: Boolean?,
    @JsonProperty("hideInstructions")
    var hideInstructions: Boolean?,
    @JsonProperty("hidePrice")
    var hidePrice: Boolean?,
    @JsonProperty("hideReviews")
    var hideReviews: Boolean?,
    @JsonProperty("id")
    var id: Int?,
    @JsonProperty("images")
    var images: List<Image?>?,
    @JsonProperty("isNew")
    var isNew: Boolean?,
    @JsonProperty("labelUrl")
    var labelUrl: String?,
    @JsonProperty("listingDescription")
    var listingDescription: List<ListingDescription?>?,
    @JsonProperty("model")
    var model: String?,
    @JsonProperty("moscowNormalizedSoldAmount")
    var moscowNormalizedSoldAmount: Double?,
    @JsonProperty("name")
    var name: String?,
    @JsonProperty("numOfComment")
    var numOfComment: Int?,
    @JsonProperty("oldPrice")
    var oldPrice: Int?,
    @JsonProperty("onOrderAvailable")
    var onOrderAvailable: Boolean?,
    @JsonProperty("pickupLocal")
    var pickupLocal: Boolean?,
    @JsonProperty("pickupRemote")
    var pickupRemote: Boolean?,
    @JsonProperty("preOrder")
    var preOrder: PreOrder?,
    @JsonProperty("price")
    var price: Int?,
    @JsonProperty("productExtendedBonus")
    var productExtendedBonus: Any?,
    @JsonProperty("productId")
    var productId: Int?,
    @JsonProperty("productWeight")
    var productWeight: Int?,
    @JsonProperty("rating")
    var rating: Int?,
    @JsonProperty("regionSample")
    var regionSample: Boolean?,
    @JsonProperty("regionalId")
    var regionalId: Int?,
    @JsonProperty("score")
    var score: String?,
    @JsonProperty("shields")
    var shields: Shields?,
    @JsonProperty("shopSample")
    var shopSample: Any?,
    @JsonProperty("shortName")
    var shortName: String?,
    @JsonProperty("showCase")
    var showCase: Boolean?,
    @JsonProperty("specialCreditPurchase")
    var specialCreditPurchase: Boolean?,
    @JsonProperty("type")
    var type: String?
) {
    data class Image(
        @JsonProperty("name")
        var name: String?,
        @JsonProperty("url")
        var url: String?
    )

    data class ListingDescription(
        @JsonProperty("attributeNameToValueMap")
        var attributeNameToValueMap: AttributeNameToValueMap?,
        @JsonProperty("id")
        var id: Int?,
        @JsonProperty("name")
        var name: String?
    ) {
        data class AttributeNameToValueMap(
            @JsonProperty("Длина")
            var длина: String?,
            @JsonProperty("Тип")
            var тип: String?
        )
    }

    data class PreOrder(
        @JsonProperty("buttonPreOrderAfter")
        var buttonPreOrderAfter: Any?,
        @JsonProperty("buttonPreOrderBefore")
        var buttonPreOrderBefore: Any?,
        @JsonProperty("hidePriceAnnounce")
        var hidePriceAnnounce: Boolean?,
        @JsonProperty("targetUrl")
        var targetUrl: Any?,
        @JsonProperty("timeStartOrder")
        var timeStartOrder: String?,
        @JsonProperty("timeStartPreOrder")
        var timeStartPreOrder: String?,
        @JsonProperty("timeStartShown")
        var timeStartShown: String?
    )

    data class Shields(
        @JsonProperty("INSTALLMENT")
        var iNSTALLMENT: INSTALLMENT?,
        @JsonProperty("MANUAL")
        var mANUAL: List<MANUAL?>?
    ) {
        data class INSTALLMENT(
            @JsonProperty("value")
            var value: Int?,
            @JsonProperty("visibleUserType")
            var visibleUserType: VisibleUserType?
        ) {
            data class VisibleUserType(
                @JsonProperty("admin")
                var admin: Boolean?,
                @JsonProperty("individual")
                var individual: Boolean?,
                @JsonProperty("legalEntity")
                var legalEntity: Boolean?,
                @JsonProperty("operator")
                var `operator`: Boolean?
            )
        }

        data class MANUAL(
            @JsonProperty("active")
            var active: Active?,
            @JsonProperty("code")
            var code: String?,
            @JsonProperty("image")
            var image: String?,
            @JsonProperty("label")
            var label: String?,
            @JsonProperty("linkPromo")
            var linkPromo: String?,
            @JsonProperty("visibleUserType")
            var visibleUserType: VisibleUserType?
        ) {
            data class Active(
                @JsonProperty("timeEnd")
                var timeEnd: String?,
                @JsonProperty("timeStart")
                var timeStart: String?
            )

            data class VisibleUserType(
                @JsonProperty("admin")
                var admin: Boolean?,
                @JsonProperty("individual")
                var individual: Boolean?,
                @JsonProperty("legalEntity")
                var legalEntity: Boolean?,
                @JsonProperty("operator")
                var `operator`: Boolean?
            )
        }
    }
}