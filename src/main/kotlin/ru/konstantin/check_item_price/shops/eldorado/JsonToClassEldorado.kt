data class JsonToClassEldorado(
    var buildId: String?,
    var dataManager: String?,
    var page: String?,
    var props: Props?,
    var query: QueryX?
)

data class Props(
    var initialProps: InitialProps?,
    var initialState: InitialState?,
    var isServer: Boolean?
)

class InitialProps(
)

data class InitialState(
    var auth: Auth?,
    var banners: Banners?,
    var brandZone: BrandZone?,
    var comparison: Comparison?,
    var eldoBlog: EldoBlog?,
    var eldoTube: EldoTube?,
    var footerMenu: FooterMenu?,
    var lastViewed: LastViewed?,
    var listing: ListingX?,
    var mainHits: MainHits?,
    var mainMenu: MainMenu?,
    var mainNovelties: MainNovelties?,
    var mainSaleProducts: MainSaleProducts?,
    var options: Options?,
    var productsOfTheDay: ProductsOfTheDay?,
    var region: Region?,
    var rocketItems: RocketItems?,
    var rocketItemsHits: RocketItemsHits?,
    var routing: Routing?,
    var search: Search?,
    var seo: Seo?,
    var seoms: Seoms?,
    var session: Session?,
    var shields: Shields?,
    var shipping: Shipping?,
    var token: Token?,
    var topMenu: TopMenu?,
    var wishlist: Wishlist?
)

data class Auth(
    var captcha: Boolean?,
    var cart: Any?,
    var error: Any?,
    var errorsByField: List<Any?>?,
    var level: String?,
    var loaded: Boolean?,
    var loading: Boolean?,
    var user: Any?
)

data class Banners(
    var error: Any?,
    var items: Items?,
    var loaded: Boolean?,
    var loading: Boolean?
)

class Items(
)

data class BrandZone(
    var listing: Listing?,
    var main: Main?
)

data class Listing(
    var error: Any?,
    var items: List<Any?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class Main(
    var error: Any?,
    var items: List<Any?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class Comparison(
    var items: List<Any?>?,
    var loaded: Boolean?
)

data class EldoBlog(
    var error: Any?,
    var items: List<Any?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class EldoTube(
    var error: Any?,
    var items: List<Any?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class FooterMenu(
    var error: Any?,
    var list: List<X?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class X(
    var children: List<Children?>?,
    var itemIndex: String?,
    var itemType: String?,
    var link: String?,
    var params: ParamsX?,
    var permission: String?,
    var selected: String?,
    var title: String?
)

data class Children(
    var itemIndex: String?,
    var itemType: String?,
    var link: String?,
    var params: Params?,
    var permission: String?,
    var selected: String?,
    var title: String?
)

data class Params(
    var depth: String?,
    var new_window: String?
)

data class ParamsX(
    var depth: String?,
    var name: String?
)

data class LastViewed(
    var items: List<Any?>?
)

data class ListingX(
    var `data`: Data?,
    var error: Any?,
    var isOpenFilter: Boolean?,
    var lastListingQuery: LastListingQuery?,
    var loaded: Boolean?,
    var loading: Boolean?,
    var selectedFilter: SelectedFilter?,
    var seoHowToChoose: Any?,
    var showMoreInProgress: Boolean?,
    var tmpLastListingQuery: Any?,
    var tmpSemData: Any?,
    var view: String?
)

data class Data(
    var classificationCategoryId: Any?,
    var correctedSearchQuery: String?,
    var `data`: DataX?,
    var fullTextSearchableCategoriesWithProducts: String?,
    var limit: Int?,
    var offset: Int?,
    var searchQuery: String?,
    var selectedTags: List<Any?>?,
    var sort: List<Sort?>?,
    var totalCount: String?
)

class DataX(
)

data class Sort(
    var direction: String?,
    var `property`: String?
)

data class LastListingQuery(
    var category: Int?,
    var factoryCode: String?,
    var limit: Int?,
    var offset: Int?
)

class SelectedFilter(
)

data class MainHits(
    var error: Any?,
    var items: Any?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class MainMenu(
    var error: Any?,
    var list: List<Any?>?,
    var loaded: Boolean?,
    var loading: Boolean?,
    var raw: List<Raw?>?
)

data class Raw(
    var breadCrumbs: List<Any?>?,
    var iconUrl: String?,
    var id: Int?,
    var isCatalogItem: Boolean?,
    var isMenuItem: Boolean?,
    var isRootMenuItem: Boolean?,
    var items: List<Item?>?,
    var label: String?,
    var link: String?,
    var parent: Any?,
    var pictureUrl: String?,
    var scopes: Int?,
    var title: String?
)

data class Item(
    var breadCrumbs: List<Any?>?,
    var iconUrl: Any?,
    var id: Any?,
    var isCatalogItem: Boolean?,
    var isMenuItem: Boolean?,
    var isRootMenuItem: Boolean?,
    var items: List<ItemX?>?,
    var label: String?,
    var link: String?,
    var parent: String?,
    var pictureUrl: Any?,
    var scopes: Int?,
    var title: String?
)

data class ItemX(
    var breadCrumbs: List<BreadCrumb?>?,
    var iconUrl: Any?,
    var id: Any?,
    var isCatalogItem: Boolean?,
    var isMenuItem: Boolean?,
    var isRootMenuItem: Boolean?,
    var items: List<Any?>?,
    var label: String?,
    var link: String?,
    var parent: String?,
    var pictureUrl: Any?,
    var scopes: Int?,
    var title: String?
)

data class BreadCrumb(
    var id: Any?,
    var text: String?,
    var url: String?
)

data class MainNovelties(
    var error: Any?,
    var items: List<Any?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class MainSaleProducts(
    var error: Any?,
    var items: Any?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class Options(
    var error: Any?,
    var loaded: Boolean?,
    var loading: Boolean?,
    var options: OptionsX?
)

data class OptionsX(
    var CALLBACK_ACTIVE: Boolean?,
    var HEADER_CALLBACK_ACTIVE: Boolean?
)

data class ProductsOfTheDay(
    var error: Any?,
    var items: List<ItemXX?>?,
    var loaded: Boolean?
)

data class ItemXX(
    var brandName: String?,
    var categoryId: Int?,
    var categoryName: String?,
    var discountValue: Int?,
    var id: Int?,
    var images: String?,
    var isAvailable: Boolean?,
    var name: String?,
    var numOfComment: Int?,
    var oldPrice: Int?,
    var price: Int?,
    var rating: Int?,
    var sort: Int?,
    var url: String?
)

data class Region(
        var currentRegionId: String?,
        var error: Any?,
        var list: List<Any?>?,
        var loaded: Boolean?,
        var loading: Boolean?,
        var regionSiteId: String?,
        var serverTime: String?,
        var timeZone: Any?,
        var virtualFactoryCode: String?
)

data class List<T>(
    var cities: List<City?>?,
    var federalDistricts: List<FederalDistrict?>?
)

data class City(
    var code: String?,
    var factoryName: String?,
    var id: String?,
    var location: String?,
    var regionSiteId: String?,
    var selfDeliveryTimeThreshold: String?,
    var timezoneOffset: String?,
    var title: String?
)

data class FederalDistrict(
    var code: String?,
    var factoryName: String?,
    var id: String?,
    var regionSiteId: String?,
    var regions: List<RegionX?>?,
    var selfDeliveryTimeThreshold: String?,
    var title: String?
)

data class RegionX(
    var code: String?,
    var factoryName: String?,
    var id: String?,
    var location: String?,
    var regionSiteId: String?,
    var selfDeliveryTimeThreshold: String?,
    var title: String?
)

data class RocketItems(
    var error: Any?,
    var items: List<Any?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class RocketItemsHits(
    var error: Any?,
    var items: List<Any?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class Routing(
    var `as`: String?,
    var cleanAs: String?,
    var page: String?,
    var pathname: String?,
    var query: Query?
)

data class Query(
    var `0`: String?
)

data class Search(
    var categories: Categories?,
    var suggestions: Suggestions?
)

data class Categories(
    var error: Any?,
    var items: List<ItemXXX?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class ItemXXX(
    var categoryId: Int?,
    var categoryName: String?,
    var categoryUri: Int?,
    var categoryWeight: Int?,
    var fullTextSearchable: Boolean?,
    var haveAvailableProducts: Any?,
    var hideMenu: Boolean?,
    var id: Int?,
    var path: String?,
    var picture: String?,
    var regionVisible: List<Any?>?,
    var restrictSort: Int?,
    var smallPicture: String?,
    var sortValue: Int?,
    var suggestionWeight: Int?,
    var svgUrl: String?,
    var type: String?
)

data class Suggestions(
    var error: Any?,
    var items: List<Any?>?
)

data class Seo(
    var error: Any?,
    var items: ItemsX?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class ItemsX(
    var description: String?,
    var keywords: String?,
    var title: String?
)

data class Seoms(
    var error: Any?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class Session(
    var SID: Any?,
    var UID: Any?,
    var error: Any?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class Shields(
    var entities: Entities?,
    var error: Any?,
    var loadedSku: List<Any?>?,
    var loadingSku: List<Any?>?
)

class Entities(
)

data class Shipping(
    var entities: EntitiesX?,
    var error: Any?,
    var loadedSku: List<Any?>?,
    var loadingSku: List<Any?>?
)

class EntitiesX(
)

data class Token(
    var error: Any?,
    var loaded: Boolean?,
    var loading: Boolean?,
    var token: String?
)

data class TopMenu(
    var error: Any?,
    var list: List<XX?>?,
    var loaded: Boolean?,
    var loading: Boolean?
)

data class XX(
    var icon: String?,
    var iconHover: String?,
    var link: String?,
    var target: Boolean?,
    var title: String?
)

data class Wishlist(
    var error: Boolean?,
    var list: ListX?,
    var loaded: Boolean?,
    var loading: Boolean?
)

class ListX(
)

data class QueryX(
    var `0`: String?
)