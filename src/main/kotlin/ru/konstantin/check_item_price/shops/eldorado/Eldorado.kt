package ru.konstantin.check_item_price.shops.eldorado

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.beust.klaxon.Parser
import com.google.gson.JsonParser
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.json.JSONObject
import org.jsoup.Jsoup
import ru.konstantin.check_item_price.network.SendRequest
import ru.konstantin.check_item_price.shops.Item
import ru.konstantin.check_item_price.shops.eldorado.fromJson.v2.JsonToProductEldorado
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Duration
import java.util.*


class Eldorado {
    val logger: Logger = LogManager.getLogger(Eldorado::class.java)
    var allRequestCount = 1
    var requestCountForOneProxy = 1
    var proxy = ""
    val shopId = 2

    suspend fun start(cityName: String) {

        val startDate = Date()
        var pageSource = getPage("https://www.eldorado.ru/d/")["200"] ?: ""

        val items = mutableSetOf<JSONObject>()
        val gItems = mutableSetOf<com.google.gson.JsonObject>()
        val categoryLinks = findAllCategoryLinks(pageSource)
//        val jobs = mutableListOf<Job>()
        for ((index, cl) in categoryLinks.withIndex()) {
//            val job = GlobalScope.launch {
//                delay((500..5000).random().toLong())
            val res = getPage("https://www.eldorado.ru${cl}")
            pageSource = res["200"] ?: ""
//            val realUrl = res["200"]?:""//неправильно

            //ПОлучаем количество страницй
            val pageCount = try {
                Jsoup.parse(pageSource).select(" div > div > div > div > div > div > ul > li:nth-last-child(2) > a[role='button']")[0].text().toInt()
            } catch (e: Exception) {
                //Исключение появляется если на сайте нет пагинатора, поэтому берем товары прямо с этой страницы
                1
            }

//            val parser: Parser = Parser.default()
            var tempUrl = ""
            for (pageNumber in 1..pageCount) {
                //Если одна страника в категории, то парсим страницу из ответа полученного выше, если нет, то делаем запрос к страницам, начиная со второй, так как первая уже получена
//                logger.info("ELDORADO ")
                val defaultSchemeItemsSet = mutableSetOf<Item>()

                pageSource = if (pageNumber == 1) {
                    pageSource
                } else {
                    tempUrl = "https://www.eldorado.ru${cl}?page=$pageNumber"
                    getPage(tempUrl)["200"] ?: ""//неправильно
                }

                try {
                    var elem = Jsoup.parse(pageSource).getElementById("__NEXT_DATA__").data()
                    val gsonTest = JsonParser.parseString(elem)
                    val needed = gsonTest.asJsonObject
                            .getAsJsonObject("props")
                            .getAsJsonObject("initialState")
                            .getAsJsonObject("products")
                            .getAsJsonObject("products")

                    if (needed != null) {
                        for (gItem in needed.entrySet()) {
                            val ggI = gItem.value.asJsonObject
                            gItems.add(ggI)

                            val newItem = Item()
                            newItem.productId = ggI.get("productId").toString()
                            newItem.brandName = ggI.get("brandName").toString()
                            newItem.productName = ggI.get("name").toString()
                            newItem.price = ggI.get("price").toString().toInt().times(100)
                            newItem.oldPrice =
                                    if ((ggI.get("oldPrice")).toString().toInt().times(100) == newItem.price?.times(100)) null
                                    else (ggI.get("oldPrice")).toString().toInt().times(100)

                            newItem.cityName = cityName
                            newItem.shopId = shopId.toLong()
                            newItem.linkToItem = ggI.get("code").toString()
                            newItem.linkToImage = ""

                            defaultSchemeItemsSet.add(newItem)
                        }
                    }
                } catch (e: Exception) {
//                    println(pageSource)
                    println(tempUrl)
                    e.printStackTrace()
                    continue
                }

//                addAllItemsToDB(items, "Санкт-Петербург")
//                println()
                GlobalScope.launch {
//                    delay((500..5000).random().toLong())
                    val klaxon = Klaxon()
                    val responseStatus = SendRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet))
                    when (responseStatus) {
                        "" -> logger.info("Response status is 200. All right!!!")
                        else -> { // Note the block
                            logger.info(responseStatus)
                        }
                    }
//                SendingRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet), "http://127.0.0.1:8080/check_price_rest_sb/additems")
                }
                logger.info("ELDORADO category $index from ${categoryLinks.count()},  page - $pageNumber from $pageCount, All items ${items.count()}")
            }
//        }
//            jobs.add(job)
        }
//        jobs.forEach { it.join() }
        logger.info("ELDORADO Parsing is over ${Date().time - startDate.time}")
    }

    private suspend fun getPage(url: String): Map<String, String> {
//        var pageSource: String
        //работает на одном ip поэтому прокси не используем. Хотя можно
//        var proxy = ""
//        val proxyWorks = ProxyWorks()
        var res: Map<String, String>
        while (true) {
            try {//отдаем страницу и урл в листе
//            res = requestWithJsoup(url)

                val headers = mapOf(
                        "authority" to "www.eldorado.ru",
                        "cache-control" to "max-age=0",
                        "upgrade-insecure-requests" to "1",
                        "dnt" to "1",
                        "user-agent" to "Mozilla/5.0 (Windows NT 10.0; WOW64, AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 YaBrowser/19.12.0.358 Yowser/2.5 Safari/537.36",
                        "sec-fetch-user" to "?1",
                        "accept" to "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                        "sec-fetch-site" to "same-origin",
                        "sec-fetch-mode" to "navigate",
                        "referer" to "https://www.eldorado.ru/d/",
                        //                .headers("accept-encoding" to "gzip, deflate, br")//из-за него получаем нечитаемый ответ
                        "accept-language" to "ru,en;q=0.9",
                        "cookie" to "iRegionSectionId=11279"
                )

                res = SendRequest().okHttpClientRequest(url, headers, baseUrl = "https://www.eldorado.ru")
                res.forEach { logger.info("${it.key} -> $url") }
//            res = java11HttpClientRequest(url)
//                pageSource = res["200"] ?: ""
                allRequestCount++
                requestCountForOneProxy++
                if (res["404"] != null) {
                    logger.info("404 error")
                }
//            if (pageSource.contains("error status request") || pageSource.isEmpty()) {
//                logger.info("ELDORADO Response not 200, change proxy. $pageSource")
////                proxy = proxyWorks.getTrueProxy()
//                requestCountForOneProxy = 1
////                continue
//            }
            } catch (e: Exception) {
                e.printStackTrace()
                delay((1000..2000).random().toLong())
                continue
            }
            break
        }
        return res
    }

    private fun findAllCategoryLinks(pageSource: String): MutableSet<String> {
        val elem = Jsoup.parse(pageSource).getElementById("__NEXT_DATA__").data()
        val parser: Parser = Parser.default()
        val stringBuilder: StringBuilder = StringBuilder(elem)
        val json: JsonObject = parser.parse(stringBuilder) as JsonObject

        val raw = (json.obj("props"))
                ?.obj("initialState")
                ?.obj("mainMenu")
                ?.array<JsonObject>("raw")
        val customLinkSet = mutableSetOf<String>()
        val linkSet = mutableSetOf<String>()
        var count = 0
        if (raw != null) {
            for (r in raw) {
                r.array<JsonObject>("items")
                for (r1 in r.array<JsonObject>("items")!!) {
                    for (r2 in r1.array<JsonObject>("items")!!) {
                        for (r3 in r2.array<JsonObject>("items")!!) {
                            r3.string("customLink")?.let { customLinkSet.add(it) }
                            r3.string("link")?.let { linkSet.add(it) }
                            count++
                        }
                    }
                }
            }
        }
        return linkSet
    }

    private fun java11HttpClientRequest(url: String): List<String> {
        //https://www.dariawan.com/tutorials/java/java-11-standard-http-client-vs-apache-httpclient/
        var httpClient = HttpClient.newBuilder()
                .followRedirects(HttpClient.Redirect.NORMAL)
                .version(HttpClient.Version.HTTP_2)
                .build()

        val reqGet = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .timeout(Duration.ofSeconds(15))
                .header("authority", "www.eldorado.ru")
                .header("cache-control", "max-age=0")
                .header("upgrade-insecure-requests", "1")
                .header("dnt", "1")
                .header("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 YaBrowser/19.12.0.358 Yowser/2.5 Safari/537.36")
                .header("sec-fetch-user", "?1")
                .header("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
                .header("sec-fetch-site", "same-origin")
                .header("sec-fetch-mode", "navigate")
                .header("referer", "https://www.eldorado.ru/d/")
//                .headers("accept-encoding", "gzip, deflate, br")//из-за него получаем нечитаемый ответ
                .header("accept-language", "ru,en;q=0.9")
                .header("cookie", "iRegionSectionId=11279")
                .build()

        var response: HttpResponse<String>? = null
        var pageSource: String = ""
        try {
            response = httpClient.send(reqGet, HttpResponse.BodyHandlers.ofString())
            pageSource = response.body()
            if (response.statusCode() == 404) {
                pageSource = "404"//чтобы не выбирался новый прокси, передаем страницу не содержащую ошибки, но и без товара
            } else if (response.statusCode() == 456) {
                pageSource = ""//требуется повторный запрос, прокси менять не надо
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
//        private final HttpClient httpClient = HttpClient.newBuilder()
//                .authenticator(new Authenticator() {
//                    @Override
//                    protected PasswordAuthentication getPasswordAuthentication() {
//                        return new PasswordAuthentication(
//                                "user",
//                        "password".toCharArray());
//                    }
//                })
//                .build();
        return listOf(pageSource, response?.uri().toString())
    }
}