package ru.konstantin.check_item_price.shops

data class Item(
        var productId: String = "",
        var brandName: String = "",
        var productName: String = "",
        var description: String = "",
        var oldPrice: Int? = null,
        var price: Int? = null,
        var cityName: String = "",
        var shopId: Long? = null,
        var linkToItem: String = "",
        var linkToImage: String = ""
) {
    override fun toString(): String {
        return "Item(productId='$productId', brandName='$brandName', productName='$productName', description='$description', oldPrice=$oldPrice, price=$price, cityName='$cityName', shopId=$shopId, linkToItem='$linkToItem', linkToImage='$linkToImage')"
    }
}