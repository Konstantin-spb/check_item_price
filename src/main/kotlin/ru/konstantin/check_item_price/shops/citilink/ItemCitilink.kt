package ru.konstantin.check_item_price.shops.citilink

data class ItemCitilink(
        var brandName: String?,
        var categoryId: Int?,
        var categoryName: String?,
        var clubPrice: Int?,
        var fakeOldPrice: Int?,
        var id: String?,
        var price: Int?,
        var shortName: String?
)

