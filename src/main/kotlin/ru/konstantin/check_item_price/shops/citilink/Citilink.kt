package ru.konstantin.check_item_price.shops.citilink

import com.beust.klaxon.Klaxon
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import ru.konstantin.check_item_price.network.ProxyWorks
import ru.konstantin.check_item_price.network.SendRequest
import java.util.*
import java.util.stream.Collectors

//@Component
class Citilink {
////        .setUserAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 YaBrowser/19.10.3.281 Yowser/2.5 Safari/537.36")
////        .setUserAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.99 Safari/537.36 Viv/2.9.1705.41")
//        .setUserAgent("Mozilla/5.0 (compatible; YandexMarket/2.0; +http://yandex.com/bots)")

    var proxy = ""

    lateinit var cityName: String
//    lateinit var cookieSpace: String
    var allRequestCount = 1
    var requestCountForOneProxy = 1
    val shopId = 1
    val logger = LogManager.getLogger(Citilink::class.java)

//    val headers = mapOf(
//            "cookie" to "_space=spb_cl%3A",
//            "referer" to "https://www.citilink.ru/",
//            "sec-fetch-mode" to "navigate",
//            "sec-fetch-site" to "same-origin",
//            "sec-fetch-user" to "?1",
//            "upgrade-insecure-requests" to "1",
//            "user-agent" to "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (ntu Chromium/78.0.3904.108 Chrome/78.0.3904.108 Safari/537.36"
//    )

    suspend fun start(cityName: String) {
        val startDate = Date()
        this.cityName = cityName
//        this.cookieSpace = cookieSpace

        var pageSource = StringBuilder(getPage("https://www.citilink.ru/catalog/").replace("<script.*?</script>".toRegex(), ""))

        //Получаем ссылки на категории товаров
//        var itemlinks = findLinks(pageSource.toString(), "li[data-category-id] a[data-category-id]")
        var itemlinks = findLinks(pageSource.toString(), "ul[class=\"category-catalog__children-list\"] > li > a")
        itemlinks = getSubCategoryLinks(itemlinks, "span[data-category-id] a")
        //For Testing
//        var itemlinks = mutableSetOf<String>("https://www.citilink.ru/catalog/for_gamers/",
//                "https://www.citilink.ru/catalog/active_sport/drones_aks/",
//                "https://www.citilink.ru/catalog/active_sport/khobbi/ruchki_3d/",
//                "https://www.citilink.ru/catalog/active_sport/khobbi/remote_control_toys/",
//                "https://www.citilink.ru/catalog/furniture/office_supplies/staplers/")
        val gson = Gson()

        for (itemLink in itemlinks) {
            while (true) {
                val items = mutableSetOf<ItemCitilink>()
                pageSource.clear()
                pageSource.append(getPage(itemLink))

                var doc: Document = Jsoup.parse(pageSource.toString())

                //получаем номер последней страницы
                //div.page_listing li.last > a[data-page] - получаем отсюда номер последней страницы
                val numberOfPages = try {
                    doc.select("div.page_listing li.last > a[data-page]")[0].attr("data-page").toInt()
                } catch (e: Exception) {
    //                e.printStackTrace()
                    //Исключение появляется если на сайте нет пагинатора, поэтому берем товары прямо с этой страницы
                    1
                }

                //Получаем все товары со страницы
                logger.info("CITILINK -> All pages in subcategory  $numberOfPages")
                if (numberOfPages > 1) {
                    for (index in 2..numberOfPages) {
                        val defaultSchemeItemsSet = mutableSetOf<ru.konstantin.check_item_price.shops.Item>()
    //                    while (true) {
                        pageSource.clear()
                        pageSource.append(getPage("${itemLink}?p=$index"))
                        items.addAll(Jsoup.parse(pageSource.toString()).select("#subcategoryList > div.product_category_list > div[data-gtm-location] > div[data-params]").map { gson.fromJson(it.attr("data-params"), ItemCitilink::class.java) })
                        //конвертируем товар в стандартный для всех магазинов
                        items.forEach {
                            val newItem = ru.konstantin.check_item_price.shops.Item()
                            newItem.productId = it.id.toString()
                            newItem.brandName = it.brandName.toString()
                            newItem.productName = it.shortName.toString()
    //                        newItem.description = it.d,
                            newItem.price = it.price?.times(100)
                            newItem.oldPrice = if (it.fakeOldPrice?.times(100) == newItem.price?.times(100)) null else it.fakeOldPrice?.times(100)
                            newItem.cityName = cityName
                            newItem.shopId = shopId.toLong()
                            newItem.linkToItem = ""
                            newItem.linkToImage = ""
                            defaultSchemeItemsSet.add(newItem)
                        }
                        logger.info("${itemLink}?p=$index. Now ${defaultSchemeItemsSet.count()} items.")
    //                    items.clear()
                        GlobalScope.launch {
                            delay((500..5000).random().toLong())
                            val klaxon = Klaxon()
                            var responseStatus = SendRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet))
                            when (responseStatus) {
                                "" -> logger.info("Response status is 200. All right!!!")
                                else -> { // Note the block
                                    logger.info(responseStatus)
                                }
                            }
                            //                SendingRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet), "http://127.0.0.1:8080/check_price_rest_sb/additems")
                        }
                        break
                    }
                } else {
                    try {
                        val defaultSchemeItemsSet = mutableSetOf<ru.konstantin.check_item_price.shops.Item>()

                        doc = Jsoup.parse(pageSource.toString())
                        pageSource.clear()
                        //Получаем все товары со страницы
                        items.addAll(doc.select("#subcategoryList > div.product_category_list > div[data-gtm-location] > div[data-params]").map { gson.fromJson(it.attr("data-params"), ItemCitilink::class.java) }.toMutableList())
                        items.forEach {
                            val newItem = ru.konstantin.check_item_price.shops.Item()
                            newItem.productId = it.id.toString()
                            newItem.brandName = it.brandName.toString()
                            newItem.productName = it.shortName.toString()
    //                        newItem.description = it.d,
                            newItem.oldPrice = it.fakeOldPrice?.times(100)
                            newItem.price = it.price?.times(100)
                            newItem.cityName = cityName
                            newItem.shopId = shopId.toLong()
                            newItem.linkToItem = ""
                            newItem.linkToImage = ""
                            defaultSchemeItemsSet.add(newItem)
                        }
                        logger.info("${itemLink}. Now ${defaultSchemeItemsSet.count()} items.")
                        items.clear()
                        GlobalScope.launch {
                            val klaxon = Klaxon()
                            SendRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet))
    //                SendingRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet), "http://127.0.0.1:8080/check_price_rest_sb/additems")
                        }
                        break
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
//        addAllItemsToDB(itemElemsSet, cityName)
//        addAllItemsToDB(defaultSchemeItemsSet, cityName)
        val endDate = Date()
        logger.info("CITILINK Parsing is over ${endDate.time - startDate.time} milliseconds.")
    }

    //получаем страницу, если получаем страницу с капчей, то меняем прокси и получем страницу снова, до тех пор пока не получим
    private suspend fun getPage(url: String): String {
        logger.info("Load $url")
        var pageSource: StringBuilder = StringBuilder()
        val proxyWorks = ProxyWorks()
        while (true) {
//            pageSource.append(requestWithJsoup(url).replace("<script.*?</script>", ""))
            try {
//                var sra = SendingRequest().apacheHttpClientRequest(url, headers, proxy = proxy)
                val headers = mapOf(
                        "cookie" to "_space=spb_cl%3A",
                        "referer" to "https://www.citilink.ru/",
                        "sec-fetch-mode" to "navigate",
                        "sec-fetch-site" to "same-origin",
                        "sec-fetch-user" to "?1",
                        "upgrade-insecure-requests" to "1",
                        "user-agent" to "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (ntu Chromium/78.0.3904.108 Chrome/78.0.3904.108 Safari/537.36"
                )
                var sra = SendRequest().okHttpClientRequest(url, headers, proxy = proxy, baseUrl = "https://www.citilink.ru")
                sra.forEach { logger.info("${it.key} -> $url") }

                val tempText: String = sra["200"] ?:""
                pageSource.append(tempText)
                allRequestCount++
                requestCountForOneProxy++
                if (pageSource.contains("error status request") || pageSource.toString().isEmpty()) {
    //                logger.info("Требуется ввод капчи, меняем прокси.")
                    logger.info("CITILINK Response not 200, change proxy. ${pageSource.toString()}")
    //                httpclient = getHttpClientWithWorksProxy()
                    proxy = proxyWorks.getTrueProxy()
//                    proxy = if (proxy.contains("http://") || proxy.contains("https://")) proxy else "http://$proxy"

                    requestCountForOneProxy = 1
                    continue
                }
                break
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return pageSource.toString()
    }

    //Получаем все ссылки категорий, в которых есть товары
    suspend fun getSubCategoryLinks(links: Set<String>, cssMask: String): MutableSet<String> {
        logger.info("Getting all item categories.")
//        var tempLinks = links.toMutableSet()
        var tempLinks = links.sorted().toMutableSet()
        val rightLinks = mutableSetOf<String>()
        val linkForCheck = mutableSetOf<String>()
        val checkedLink = mutableSetOf<String>()
        var pageSource: String
        while (tempLinks.isNotEmpty()) {
            for (link in tempLinks) {
                while (true) {
                    try {
                        pageSource = getPage(link)
                        val subItemlinks = findLinks(pageSource, cssMask)
                        if (subItemlinks.isEmpty()) {
                            rightLinks.add(link)
                        } else {
                            linkForCheck.addAll(subItemlinks)
                        }
                        checkedLink.add(link)
                        break
                    } catch (e: Exception) {
                        logger.info(e.message)
                        e.printStackTrace()
//                        mutableListOf<String>()
                    }
                }
                logger.info("Got ${linkForCheck.count()} links.")
            }
            tempLinks = (linkForCheck - rightLinks - checkedLink).toMutableSet()
        }

        return rightLinks
    }

    fun findLinks(pageSource: String, mask: String): Set<String> {
        val doc: Document = Jsoup.parse(pageSource)
//        val elems = doc.select(mask).map { it.attr("href") }
        val elems = doc.select(mask).stream().map { it.attr("href") }.collect(Collectors.toSet())
        return elems.toSet()
    }
}
