package ru.konstantin.check_item_price.shops.mvideo.fromJson.v2


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class MvideoProducts(
    @JsonProperty("data")
    var `data`: Data?
) {
    data class Data(
        @JsonProperty("products")
        var products: List<Product?>?
    ) {
        data class Product(
            @JsonProperty("brandName")
            var brandName: String?,
            @JsonProperty("category")
            var category: Category?,
            @JsonProperty("features")
            var features: Features?,
            @JsonProperty("imageURL")
            var imageURL: String?,
            @JsonProperty("name")
            var name: Name?,
            @JsonProperty("productId")
            var productId: String?,
            @JsonProperty("rating")
            var rating: Rating?,
            @JsonProperty("__typename")
            var typename: String?
        ) {
            data class Category(
                @JsonProperty("id")
                var id: String?,
                @JsonProperty("name")
                var name: String?,
                @JsonProperty("__typename")
                var typename: String?
            )

            data class Features(
                @JsonProperty("energyEfficiency")
                var energyEfficiency: List<Any?>?,
                @JsonProperty("main")
                var main: List<Main?>?,
                @JsonProperty("__typename")
                var typename: String?
            ) {
                data class Main(
                    @JsonProperty("name")
                    var name: String?,
                    @JsonProperty("__typename")
                    var typename: String?,
                    @JsonProperty("value")
                    var value: String?
                )
            }

            data class Name(
                @JsonProperty("cyrillic")
                var cyrillic: String?,
                @JsonProperty("latin")
                var latin: String?,
                @JsonProperty("__typename")
                var typename: String?
            )

            data class Rating(
                @JsonProperty("count")
                var count: Int?,
                @JsonProperty("star")
                var star: Int?,
                @JsonProperty("__typename")
                var typename: String?
            )
        }
    }
}