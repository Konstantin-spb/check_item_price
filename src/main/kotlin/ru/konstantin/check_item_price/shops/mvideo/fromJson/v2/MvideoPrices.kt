package ru.konstantin.check_item_price.shops.mvideo.fromJson.v2


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class MvideoPrices(
    @JsonProperty("data")
    var `data`: Data?
) {
    data class Data(
        @JsonProperty("prices")
        var prices: List<Price?>?
    ) {
        data class Price(
            @JsonProperty("actionPrice")
            var actionPrice: Double?,
            @JsonProperty("basePrice")
            var basePrice: Double?,
            @JsonProperty("bonusRubles")
            var bonusRubles: Double?,
            @JsonProperty("economy")
            var economy: Double?,
            @JsonProperty("productId")
            var productId: String?,
            @JsonProperty("__typename")
            var typename: String?
        )
    }
}