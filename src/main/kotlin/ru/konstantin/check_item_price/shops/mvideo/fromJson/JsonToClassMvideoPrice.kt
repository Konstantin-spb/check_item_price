package ru.konstantin.check_item_price.shops.mvideo.fromJson

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

data class JsonToClassMvideoPrice(
        var `data`: Data?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Data(
        var prices: List<Price?>?
)

data class Price(
        var __typename: String?,
        var actionPrice: Double?,
        var basePrice: Double?,
        var bonusRubles: Double?,
        var economy: Double?,
        var productId: String?
)