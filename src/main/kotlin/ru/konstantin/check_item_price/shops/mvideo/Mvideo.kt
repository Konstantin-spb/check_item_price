package ru.konstantin.check_item_price.shops.mvideo

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.beust.klaxon.Parser
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import org.jsoup.Jsoup
import ru.konstantin.check_item_price.network.SendRequest
import ru.konstantin.check_item_price.shops.Item
import ru.konstantin.check_item_price.shops.mvideo.fromJson.v2.MvideoPrices
import ru.konstantin.check_item_price.shops.mvideo.fromJson.v2.MvideoProducts
import java.util.*


//@Component
class Mvideo {
    val logger = LogManager.getLogger(Mvideo::class.java)
    var allRequestCount = 1
    var requestCountForOneProxy = 1
    var proxy = ""
//    val productSet = mutableSetOf<Product>()
//    val priceSet = mutableSetOf<Price>()
//    val defaultSchemeItemsSet = mutableSetOf<ru.konstantin.check_item_price.shops.Item>()

    //    @Autowired
//    lateinit var itemToDbService: ItemToDbService
    private val headers = mapOf<String, String>(
            "authority" to "www.mvideo.ru",
            "accept" to "*/*",
            "origin" to "https://www.mvideo.ru",
            "user-agent" to "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (ntu Chromium/78.0.3904.108 Chrome/78.0.3904.108 Safari/537.36",
            "content-type" to "application/json",
            "sec-fetch-site" to "same-origin",
            "sec-fetch-mode" to "cors",
            "referer" to "https://www.mvideo.ru/",
//        "accept-encoding" to "gzip, deflate, br",
            "accept-language" to "en-US,en;q=0.9,ru;q=0.8",
            "cookie" to "MVID_CITY_ID=CityCZ_1638"//СПб
//            "cookie" to "MVID_CITY_ID=CityCZ_1638; MVID_AB=3; abtest_IR=3"//СПб
//                "cookie" to "MVID_CITY_ID=CityCZ_975"//Москва
    )

    suspend fun start(cityName: String) {

        val startDate = Date()
        val pageSource = getPage("https://www.mvideo.ru/catalog")["200"] ?: ""

//        val items = mutableSetOf<JsonObject>()
        var categoryLinksSet = findAllCategoryLinks(pageSource)
        categoryLinksSet = getCatigoriesId(categoryLinksSet)
        categoryLinksSet.forEach {
            logger.info(it)
        }

        var data: String
        var jsonPage: String
        var allItemsId = mutableSetOf<String>()
        val parser: Parser = Parser.default()

        for (categoryId in categoryLinksSet) {
            try {//Делаем запрос и получаем id-товаров
                val queryLimit = 10000
//                data = """{"operationName":"searchListing","variables":{"condition":{"categoryId":"$categoryId","filters":[],"shopId":null,"availableNow":false},"limit":10000,"offset":0,"order":"DEFAULT"},"query":"query searchListing(${'$'}condition: SearchListingCondition!, ${'$'}order: SearchOrder, ${'$'}limit: Int!, ${'$'}offset: Int) {\n  searchListing(condition: ${'$'}condition, order: ${'$'}order, limit: ${'$'}limit, offset: ${'$'}offset) {\n    result {\n      groups {\n        total\n        items\n        filters {\n          name\n          values {\n            count\n            value\n            digit\n            name\n            url\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    seo {\n      ...seoFields\n      __typename\n    }\n    newUrl\n    breadcrumbs {\n      name\n      link\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment seoFields on SeoSetting {\n  h1\n  title\n  metaDescription\n  keywords\n  seoText\n  templatedSeoText\n  canonicalUrl\n  __typename\n}\n"}"""
                data = """{"operationName":"searchListing","variables":{"condition":{"categoryId":"$categoryId","filters":[],"shopId":null,"availableNow":false},"limit":$queryLimit,"offset":0,"order":"DEFAULT"},"query":"query searchListing(${'$'}condition: SearchListingCondition!, ${'$'}order: SearchOrder, ${'$'}limit: Int!, ${'$'}offset: Int) {\n  searchListing(condition: ${'$'}condition, order: ${'$'}order, limit: ${'$'}limit, offset: ${'$'}offset) {\n    result {\n      groups {\n        total\n        items\n        filters {\n          name\n          values {\n            count\n            value\n            name\n            url\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    seo {\n      ...seoFields\n      __typename\n    }\n    newUrl\n    breadcrumbs {\n      name\n      link\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment seoFields on SeoSetting {\n  h1\n  title\n  metaDescription\n  keywords\n  seoText\n  templatedSeoText\n  canonicalUrl\n  __typename\n}\n"}"""
                while (true) {
                    try {
                        //curl 'https://www.mvideo.ru/.rest/graphql' -H 'authority: www.mvideo.ru' -H 'accept: */*' -H 'origin: https://www.mvideo.ru' -H 'adrum: isAjax:true' -H 'user-agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.136 YaBrowser/20.2.3.213 Yowser/2.5 Yptp/1.21 Safari/537.36' -H 'dnt: 1' -H 'content-type: application/json' -H 'sec-fetch-site: same-origin' -H 'sec-fetch-mode: cors' -H 'referer: https://www.mvideo.ru/televizory-i-cifrovoe-tv-1/televizory-65' -H 'accept-encoding: gzip, deflate, br' -H 'accept-language: ru,en;q=0.9' -H $'cookie: MVID_GUEST_ID=13415278445;wurfl_device_id=generic_web_browser;searchType=2;MVID_CITY_ID=CityCZ_1638;MVID_REGION_ID=6;MVID_VIEWED_PRODUCTS=;COMPARISON_INDICATOR=false;uxs_mig=1;PaymentMethodsOnCart_2=1;MVID_DELETE_WITH_POPUP=1;NewRasProc=1;MVID_ADD_TO_CART_WITH_POPUP=1;flacktory=no;MVID_AB=2;kfp_login=no;deviceType=desktop'

                        val head = mapOf(
                                "authority" to "www.mvideo.ru",
                                "accept" to "*/*",
                                "adrum" to "isAjax:true",
                                "origin" to "https://www.mvideo.ru",
                                "user-agent" to "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (ntu Chromium/78.0.3904.108 Chrome/78.0.3904.108 Safari/537.36",
                                "content-type" to "application/json",
                                "sec-fetch-site" to "same-origin",
                                "sec-fetch-mode" to "cors",
                                "sec-fetch-dest" to "empty",
                                "referer" to "https://www.mvideo.ru/",
//                                "accept-encoding" to "gzip, deflate, br",
                                "accept-language" to "en-US,en;q=0.9,ru;q=0.8",
                                "cookie" to "MVID_CITY_ID=CityCZ_1638"
                        )
                        jsonPage = graphqlRequest(head, data)
                        break
                    } catch (e: Exception) {
                    }
                }
                val stringBuilder: StringBuilder = StringBuilder(jsonPage)
                val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                val itemsJsonArray = json.obj("data")
                        ?.obj("searchListing")
                        ?.obj("result")
                        ?.array<JsonObject>("groups")?.get(0)
                        ?.array<String>("items")?.toSet()
                if (itemsJsonArray != null) {
                    allItemsId.addAll(itemsJsonArray)
                }

                logger.info("All items - ${allItemsId.count()}")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        var allItemsByChunk = allItemsId.asSequence().chunked(100).map { group ->
            group.toList()
        }.toList()

        val mapper = ObjectMapper()
        for ((index, ls) in allItemsByChunk.withIndex()) {
            val defaultSchemeItemsSet = mutableSetOf<ru.konstantin.check_item_price.shops.Item>()

            val itemsHowString = ls.joinToString(prefix = "\"", postfix = "\"", separator = "\",\"") { it }
//            data = """{"operationName":"products","variables":{"productIds":[$itemsHowString]},"query":"query products(${'$'}productIds: [String]\u0021) {\n  products(productIds: ${'$'}productIds) {\n    productId\n    name {\n      cyrillic\n      latin\n      __typename\n    }\n    weight\n    brand {\n      name\n      seoName\n      logo\n      __typename\n    }\n    media {\n      images {\n        key\n        urls\n        __typename\n      }\n      pdpIcons {\n        text\n        url\n        __typename\n      }\n      videos\n      images {\n        key\n        urls\n        __typename\n      }\n      __typename\n    }\n    description {\n      brief\n      full\n      __typename\n    }\n    features {\n      main {\n        name\n        value\n        nameTooltipText\n        valueTooltipText\n        __typename\n      }\n      energyEfficiency {\n        name\n        value\n        __typename\n      }\n      __typename\n    }\n    rating {\n      star\n      count\n      __typename\n    }\n    features {\n      main {\n        name\n        value\n        __typename\n      }\n      energyEfficiency {\n        name\n        value\n        __typename\n      }\n      full {\n        id\n        name\n        features {\n          name\n          value\n          nameTooltipText\n          valueTooltipText\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    certificateFiles {\n      name\n      extension\n      url\n      size\n      __typename\n    }\n    categories {\n      id\n      name\n      url\n      level\n      __typename\n    }\n    links {\n      rel\n      name\n      href\n      __typename\n    }\n    __typename\n  }\n}\n"}"""
            data = """{"operationName":"products","variables":{"productIds":[$itemsHowString]},"query":"query products(${'$'}productIds: [String]!) {\n  products(productIds: ${'$'}productIds) {\n    productId\n    name {\n      cyrillic\n      latin\n      __typename\n    }\n    brandName\n    rating {\n      star\n      count\n      __typename\n    }\n    imageURL\n    features {\n      main {\n        name\n        value\n        __typename\n      }\n      energyEfficiency {\n        name\n        value\n        __typename\n      }\n      __typename\n    }\n    category {\n      id\n      name\n      __typename\n    }\n    __typename\n  }\n}\n"}"""
            var mvideoProducts: MvideoProducts?
            while (true) {
                try {
                    jsonPage = graphqlRequest(headers, data)
                    mvideoProducts = mapper.readValue(jsonPage, MvideoProducts::class.java)
                    break
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            //Здесь меняем regionId для каждого города
            //"regionId":"6" - СПб
            //"regionId":"1" - Москва
            data = """{"operationName":"prices","variables":{"productIds":[$itemsHowString],"regionId":"6"},"query":"query prices(${'$'}productIds: [String]!, ${'$'}regionId: String!) {\n  prices(productIds: ${'$'}productIds, regionId: ${'$'}regionId) {\n    productId\n    actionPrice\n    basePrice\n    economy\n    bonusRubles\n    __typename\n  }\n}\n"}"""
            var mvideoPrices: MvideoPrices?
            while (true) {
                try {
                    jsonPage = graphqlRequest(headers, data)
                    mvideoPrices = mapper.readValue(jsonPage, MvideoPrices::class.java)
//                    jsonToClassMvideoPrice = klaxon.parse<JsonToClassMvideoPrice>(jsonPage)
                    break
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            defaultSchemeItemsSet.clear()
            val productList = mvideoProducts?.data?.products?.sortedBy { it?.productId } ?: listOf()
            val priceList = mvideoPrices?.data?.prices?.sortedBy { it?.productId } ?: listOf()
            for (product in productList) {
                for (price in priceList) {
                    if (product?.productId.equals(price?.productId, true)) {
                        try {
                            val newItem = Item()
                            newItem.productId = product?.productId ?: ""
                            newItem.brandName = product?.brandName ?: ""
                            newItem.productName = product?.name?.cyrillic ?: ""
//                            newItem.description = product?.description?.full ?: ""

                            newItem.price = (price?.actionPrice?.toFloat()?.times(100))?.toInt()
                            newItem.oldPrice =
                                    if ((price?.basePrice?.toFloat()?.times(100))?.toInt() == (price?.actionPrice?.toFloat()?.times(100))?.toInt()) null else (price?.basePrice?.toFloat()?.times(100))?.toInt()
                            newItem.shopId = 3
                            newItem.cityName = cityName

                            newItem.linkToItem = try {
                                "https://www.mvideo.ru/products/${product?.name?.latin}"
                            } catch (e: Exception) {
                                ""
                            }
                            newItem.linkToImage = try {
                                product?.imageURL ?: ""
                            } catch (e: Exception) {
                                ""
                            }
                            defaultSchemeItemsSet.add(newItem)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            logger.info("Added $index items.")
            GlobalScope.launch {
                delay((500..5000).random().toLong())
                val klaxon = Klaxon()
                val responseStatus = SendRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet))
                when (responseStatus) {
                    "" -> logger.info("Response status is 200. All right!!!")
                    else -> { // Note the block
                        logger.info(responseStatus)
                    }
                }
//                SendingRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet), "http://127.0.0.1:8080/check_price_rest_sb/additems")
            }
        }

        logger.info("mvideo Parsing is over ${Date().time - startDate.time}")
    }

    private fun getCatigoriesId(categoryLinksSet: Set<String>): Set<String> {
        var categoryLinksSet1 = categoryLinksSet
        var categoryLinksList: MutableList<String>
        categoryLinksList = categoryLinksSet1
                .filter { !it.contains("/promo/") }
                .map {
                    when {
                        it.contains("/f/") -> {
                            it.substringBefore("/f/")
                        }
                        it.contains("?reff") -> {
                            it.substringBefore("?reff")
                        }
                        else -> {
                            it
                        }
                    }
                }.toMutableList()//^/[\w-]+/[\w-]+\s

        categoryLinksSet1 = categoryLinksList.filter {
            !it.contains("http://")
                    && !it.contains("https://")
                    && !it.contains("?from")
                    && (it.endsWith("0")
                    || it.endsWith("1")
                    || it.endsWith("2")
                    || it.endsWith("3")
                    || it.endsWith("4")
                    || it.endsWith("5")
                    || it.endsWith("6")
                    || it.endsWith("7")
                    || it.endsWith("8")
                    || it.endsWith("9")
                    )
        }
                .map { it.substringAfterLast("-") }
                .toSet()
        return categoryLinksSet1
    }

    private suspend fun getPage(url: String): Map<String, String> {
        var pageSource: String
        //работает на одном ip поэтому прокси не используем. Хлтя можно
//        var proxy = ""
//        val proxyWorks = ProxyWorks()
        var res: Map<String, String>
        while (true) {
            //отдаем страницу и урл в листе
//            res = requestWithJsoup(url)
//            res = java11HttpClientRequest(url, mapOf())
            try {
                //может кидать эксепшены
                res = SendRequest().okHttpClientRequest(url, headers, baseUrl = "https://www.mvideo.ru")
                res.forEach { logger.info("${it.key} -> $url") }
            } catch (e: Exception) {
                continue
            }
            pageSource = res["200"] ?: ""
            logger.info("m-video All requestcount = $allRequestCount $url" +
                    " , cделано $requestCountForOneProxy для этого прокси.")
            allRequestCount++
            requestCountForOneProxy++
            if (pageSource.contains("error status request") || pageSource.isEmpty()) {
//                logger.info("Требуется ввод капчи, меняем прокси.")
                logger.info("m-video Response not 200, change proxy. $pageSource")
//                proxy = proxyWorks.getTrueProxy()

                requestCountForOneProxy = 1
                //Если код ответа равен нулю, то это значит зацикленный редирект, больше по этой ссылке не переходим.
//                if (res.containsKey("0")) {
//                    logger.info("")
//                    break
//                }
                continue
            }
            break
        }
        return res
    }

    private fun findAllCategoryLinks(pageSource: String): Set<String> {
        //Получаем все ссылки на категории в главном меню, кроме акций
        val linkSet = Jsoup
                .parse(pageSource)
//                .select("li.header-nav-item:not(.hni-actions) > div > div.header-nav-drop-down-holder > ul.header-nav-drop-down-list > li.header-nav-drop-down-column > ul.header-nav-drop-down-column-list > li.header-nav-drop-down-list-item > a")
                .select("nav > ul > li > div > div > ul > li > ul > li > a")
                .map { it.attr("href") }.toSet()

        linkSet.toList().sorted().forEach { println(it) }
        return linkSet
    }

    //Получаем количество и все id товаров по заданной категории
    private suspend fun graphqlRequest(headers: Map<String, String>, data: String): String {
        val response = SendRequest().okHttpClientRequest("https://www.mvideo.ru/.rest/graphql", headers, "post", proxy, data, baseUrl = "https://www.mvideo.ru/")
        return response["200"] ?: ""
    }
}