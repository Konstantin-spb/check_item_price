package ru.konstantin.check_item_price.shops

interface Shop {
    fun findAllLinks(cssElemsMask: String, pageSource: String): Set<String>

//    fun findPaginator(): List<String>

    fun getSourcePage()

    fun findAllItemsFromPage(): List<Item>

    fun sendItemsToServer(items: List<Item>) {
//        GlobalScope.launch {
//            val klaxon = Klaxon()
//            var responseStatus = SendingRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet))
//            when (responseStatus) {
//                "" -> logger.info("Response status is 200. All right!!!")
//                else -> { // Note the block
//                    logger.info(responseStatus)
//                }
//            }
//            responseStatus = ""
//        }
    }
}