package ru.konstantin.check_item_price.shops.m220volt

import org.jsoup.Jsoup
import ru.konstantin.check_item_price.network.SendRequest
import ru.konstantin.check_item_price.shops.Item
import ru.konstantin.check_item_price.shops.Shop

class M220Volt : Shop {
    fun run() {
        val startCategoryUrl = "https://www.220-volt.ru/catalog/"
        val sr = SendRequest()
//        sr.apacheHttpClientRequest(startCategoryUrl, )
    }

    override fun findAllLinks(cssElemsMask: String, pageSource: String): Set<String> {
        val elems = Jsoup.parse(pageSource).select(cssElemsMask)
        var links = setOf<String>()
        links = elems.map { it.attr("href") }.toSet()
        return links
    }

    override fun getSourcePage() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAllItemsFromPage(): List<Item> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /*
    Если на странице нет (//a[contains(@class, 'js-load-more')]), значит нет пагинатора.
    Если пагинатор есть, то ссылки для перехода https://www.220-volt.ru/catalog/akkumulyatornye-otvertky/?p=15 (шаг страницы 15
    https://www.220-volt.ru/catalog/akkumulyatornye-otvertky/?p=30)
     */
}

/**
 * 1.Изначально имеем страницу со всеми ссылками на категории(основное меню);
 * 2.Получаем все уникальные ссылки на категории товаров;
 * 3.Переходим по первой ссылке категории и получаем все товары;
 * 4.Если есть дополнительные страницы с товарами, то получаем товары со всех страниц;
 */

//curl 'https://www.220-volt.ru/catalog/'
//-H 'Connection: keep-alive'
//-H 'Cache-Control: max-age=0'
//-H 'Upgrade-Insecure-Requests: 1'
//-H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
//-H 'Sec-Fetch-User: ?1'
//-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'
//-H 'Sec-Fetch-Site: none'
//-H 'Sec-Fetch-Mode: navigate'
//-H 'Accept-Encoding: gzip, deflate, br'
//-H 'Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
//-H 'Cookie: rerf=AAAAAF5XnnpjY9vnAwygAg==
//ipp_uid2=bUmviNMfQ0Tbjquo/RK+pCfdn1c/idJ1t3jyFrQ==
//ipp_uid1=1582800506716
//ipp_uid=1582800506716/bUmviNMfQ0Tbjquo/RK+pCfdn1c/idJ1t3jyFrQ==
//advref=typein:
//advref_first=typein:
//client_timestamp=1582800554
//session=nLnOaNnTlKc51JXDTDMBqx
//ab_sort_listing=0
//telemetryToken=nLnOaNnTlKc51JXDTDMBqxbKXxEAmn33Lc11JgQBMY0N
//backendStart=1582822261625
//backendEnd=1582822262638
//ipp_key=v1582822261616/v3394bd400b5e53a13cfc65163aeca6afa04ab3/A60S62gvkoI+SHCqjE8Drw==' --compressed
//city_code=7700000000000;