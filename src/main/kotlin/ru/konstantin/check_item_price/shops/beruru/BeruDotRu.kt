package ru.konstantin.check_item_price.shops.beruru

import com.beust.klaxon.Klaxon
import com.google.gson.JsonParser
import kotlinx.coroutines.*
import org.apache.http.client.utils.URLEncodedUtils
import org.apache.http.message.BasicNameValuePair
import org.apache.logging.log4j.LogManager
import org.jsoup.Jsoup
import org.openqa.selenium.By
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.interactions.Actions
import ru.konstantin.check_item_price.network.SendRequest
import ru.konstantin.check_item_price.shops.Item
import java.net.URI
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.TimeUnit


//@Component
class BeruDotRu {
    private val sendRequest = SendRequest()
    val logger = LogManager.getLogger(BeruDotRu::class.java)
    var allRequestCount = 1
    var requestCountForOneProxy = 1
    val allLinks = mutableMapOf<String, Boolean>()


//    private val headers = mapOf<String, String>(
//            "Connection" to "keep-alive",
//            "Upgrade-Insecure-Requests" to "1",
//            "DNT" to "1",
//            "User-Agent" to "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 YaBrowser/20.6.0.905 Yowser/2.5 Yptp/1.23 Safari/537.36",
//            "Accept" to "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
//            "Sec-Fetch-Site" to "same-origin",
//            "Sec-Fetch-Mode" to "navigate",
//            "Sec-Fetch-User" to "?1",
//            "Sec-Fetch-Dest" to "document",
//            "Referer" to "https://beru.ru/",
//            "Accept-Language" to "ru,en;q=0.9",
//            "Cookie" to "mda=1; Cookie_check=checked; skid=337579681591291287; _ym_d=1591291288; _ym_uid=1591291288630663706; gdpr=0; yandexuid=6415760031588084319; yandex_login=adrenaline1; i=+aMMJMliFXccBe0Gxwv47K+b8BuqJXXlOQLKZG8E6ilHGdC/9qG5bzf0zSBGAveM0BGsTBWNLCwaMzbmKgi8CTJRmQA=; region_dropdown_showed=true; gdpr=0; fonts-loaded=1; uid=AABam17q+rqFaADFBNStAg==; js=1; acclinks=015554263; visits=1591291287-1592457914-1592562386; available-delivery=2%3D1; yp=1592648788.yu.6415760031588084319; ymex=1595154388.oyu.6415760031588084319; Session_id=3:1592562388.5.0.1588084515105:U6gSBQ:14.1|5554263.0.2|45:19330.115485.zLyGbEwGF4g0MW7jJMHqrKMb1ow; ys=udn.cDphZHJlbmFsaW5lMQ%3D%3D#c_chck.3425588279; mda2_beacon=1592562388444; sso_status=sso.passport.yandex.ru:synchronized; parent_reqid_seq=8e212edbb490fbff583ba7e3aae0a1e3%2C5785a5b38a3be1508bc8d1d96efdace5%2Ce6d2d36902b98ed47d7d3d0fbbd94b4c%2Cd8dee67d33eb3f8ca6d6213fa10fcc8b%2C343748279ba14ab745ec8a0840d0243a; last-loaded-page-id=blue-market%3Aindex"
//    )

    fun start(cityName: String) {
        //Первый запрос для получения memento, первый ключ для последующих запросов
        val startDate = Date()
        val categoryLinksSet = getNeededCategoryLinks()
//        var categoryLinksSet = setOf("https://beru.ru/catalog/shiny-i-diski/76935?hid=10613544")
        categoryLinksSet.forEach {
            allLinks[it] = false
            logger.info(it)
        }

        val allItemsId = mutableSetOf<String>()
        val defaultSchemeItemsSet = mutableSetOf<Item>()
        while (allLinks.filter { !it.value }.count() > 0 ) {
            try {
                defaultSchemeItemsSet.clear()
                val headers = mapOf(
                        "Connection" to "keep-alive",
                        "Upgrade-Insecure-Requests" to "1",
                        "DNT" to "1",
                        "User-Agent" to "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.101 YaBrowser/20.7.0.894 Yowser/2.5 Yptp/1.23 Safari/537.36",
                        "Accept" to "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                        "Sec-Fetch-Site" to "same-origin",
                        "Sec-Fetch-Mode" to "navigate",
                        "Sec-Fetch-User" to "?1",
                        "Sec-Fetch-Dest" to "document",
                        "Referer" to "https://beru.ru/",
                        "Accept-Language" to "ru,en;q=0.9"
                )
                val link = getRandomLink()
                val res = Jsoup.connect(link)
                        .headers(headers)
                        .execute()

                //Присваиваем ссылке true, так как ее уже использовали.
                allLinks[link] = true
                if (res.statusCode() == 200) {
                    val pageSourceDoc = res.parse()
                    val haveMorePage = pageSourceDoc.select("button[data-auto=\"pager-more\"]").text().isNotBlank()
                    val params = URLEncodedUtils.parse(URI(link), Charset.forName("UTF-8")).toMutableList()

                    if (haveMorePage) {
                        if (link.contains("page=")) {
                            for ((index, param) in params.withIndex()) {
                                if (param.name.equals("page", true)) {
                                    params.set(index, BasicNameValuePair("page", (param.value.toInt() + 1).toString()))
                                    val nextPageLink = "${link.substringBefore("?")}?${URLEncodedUtils.format(params, Charset.forName("UTF-8"))}"
//                                    val nextPageLink =
//                                            "${link.substringBefore("?")}&page=${param.split("=")[1].toInt() + 1}"
                                    allLinks[nextPageLink] = false
                                    logger.info(nextPageLink)
                                }
                            }
                        } else {
                            params.add(BasicNameValuePair("page", "2"))
                            val nextPageLink = "${link.substringBefore("?")}?${URLEncodedUtils.format(params, Charset.forName("UTF-8"))}"
                            allLinks[nextPageLink] = false
                            logger.info(nextPageLink)
                        }
                    }
                    logger.info("${haveMorePage} - > ${link}")
                    val allScriptElems = pageSourceDoc.select("script.apiary-patch[type='application/json']").filter { it.toString().contains("{\"widgets\":{\"@marketplace/SearchSerp\"") }
                    var json: String = ""
                    if (allScriptElems.count() > 0) {
                        json = allScriptElems[0].toString().substringAfter(">").substringBeforeLast("<")
                        if (json.isBlank()) {
//                            continue
                        }
                    } else {
//                        continue
                    }
                    val gsonTest = JsonParser.parseString(json)
                    val needed = gsonTest.asJsonObject
                            .getAsJsonObject("collections")
                            .getAsJsonObject("offer")

                    //получаем данные каждого элемента: название, цена и тд.
                    for (item in needed.entrySet()) {
                        val el = item.value.asJsonObject

                        val newItem = Item()
                        newItem.productId = el.get("productId").asString
                        newItem.productName = el.getAsJsonObject("titles").get("raw").asString
                        newItem.price = (el.getAsJsonObject("price").get("value").asString?.toFloat()?.times(100))?.toInt() ?: 0
                        newItem.shopId = 4
                        newItem.cityName = cityName

                        newItem.linkToItem = try {
                            el.get("slug").asString
                        } catch (e: Exception) {
                            ""
                        }
                        defaultSchemeItemsSet.add(newItem)
                        allItemsId.add(newItem.productId)
                    }

                    val scope = CoroutineScope(Job() + Dispatchers.Default)
                    scope.async {
                            val klaxon = Klaxon()
                            val responseStatus = SendRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet))
                            when (responseStatus) {
                                "" -> logger.info("Response status is 200. All right!!!")
                                else -> { // Note the block
                                    logger.info(responseStatus)
                                }
                            }
//                SendingRequest().sendJsonPost(klaxon.toJsonString(defaultSchemeItemsSet), "http://127.0.0.1:8080/check_price_rest_sb/additems")
                    }

                    logger.info("All items - ${allItemsId.count()}. Links true = ${allLinks.filter { it.value }.count()}, false = ${allLinks.filter { !it.value }.count()}, all = ${allLinks.count()} ")
                } else {
                    logger.info("Some error. ${res.statusCode()} -> ${res.statusMessage()}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

//        var allItemsByChunk = allItemsId.asSequence().chunked(100).map { group ->
//            group.toList()
//        }.toList()
        logger.info("BeruDotRu Parsing is over ${Date().time - startDate.time}")
    }

    private fun getNeededCategoryLinks(): List<String> {
        val chromeOptions = ChromeOptions()
        chromeOptions.addArguments("--disable-gpu"/*, "--blink-settings=imagesEnabled=false"*/)
//        val prefs: MutableMap<String, Any> = HashMap()
        val driver = ChromeDriver(chromeOptions)
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS)
        driver.manage().deleteAllCookies()

        driver.get("https://beru.ru")

        driver.findElement(By.xpath("//div[@data-zone-name='headerCatalog']/span/button[@type='button']")).click()
        val neededCategoryLinks = mutableSetOf<String>()
        val elems = driver.findElements(By.xpath("//ul/li/div/div/a[@data-tid]"))
        val action = Actions(driver)
        for (elem in elems) {
            action.moveToElement(elem).build().perform()
            driver.findElements(By.xpath("//div[@data-auto='subMenu']//a"))
                    .forEach { neededCategoryLinks.add(it.getAttribute("href")) }
        }
        driver.quit()
        return neededCategoryLinks.shuffled()
    }

    private fun getRandomLink(): String {
        return allLinks.filter { !it.value }.entries.random().key
    }
}