package ru.konstantin.check_item_price.shops.yandex_market


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class YandexItem(
    @SerialName("anaplanIds")
    var anaplanIds: List<String>? = null,
    @SerialName("hid")
    var hid: Int? = null,
    @SerialName("isGiftAvailable")
    var isGiftAvailable: Boolean? = null,
    @SerialName("marketSkuCreator")
    var marketSkuCreator: String? = null,
    @SerialName("oldPrice")
    var oldPrice: Int? = null,
    @SerialName("paddingBottom")
    var paddingBottom: String? = null,
    @SerialName("paddingTop")
    var paddingTop: String? = null,
    @SerialName("price")
    var price: Int? = null,
    @SerialName("productId")
    var productId: Int? = null,
    @SerialName("promos")
    var promos: List<String>? = null,
    @SerialName("rate")
    var rate: Double? = null,
    @SerialName("responses")
    var responses: Int? = null,
    @SerialName("skuId")
    var skuId: String? = null,
    @SerialName("slug")
    var slug: String? = null
)