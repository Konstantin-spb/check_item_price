package ru.konstantin.check_item_price.shops.yandex_market

import com.beust.klaxon.Klaxon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonObject
import org.apache.logging.log4j.LogManager
import org.jsoup.Jsoup
import org.openqa.selenium.By
import org.openqa.selenium.Proxy
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.seimicrawler.xpath.JXDocument
import ru.konstantin.check_item_price.network.SendRequest
import ru.konstantin.check_item_price.parsers.ParseHtmlPage
import ru.konstantin.check_item_price.shops.Item
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * https://market.yandex.ru/catalog--karty-pamiati/58444/list?cpa=1&hid=91032&fesh=431782
 * cpa=1 - с возможностью купить только на Яндекс маркете
 * fesh=431782 - товары только с Яндекс маркета
 * onstock=0 - в продаже = 1, иначе 0
 */
data class YandexWebEntity(var link: String, var alreadyUsed: Boolean = false)

class YandexMarket {
    private val logger = LogManager.getLogger(YandexMarket::class.java)

    private var allTypeLinks = mutableSetOf<YandexWebEntity>()
    private val jsonParser = Json {
        ignoreUnknownKeys = true
    }

    //    val proxyList = getProxyListFromFile("proxies.txt")
    private val parseHtmlPage = ParseHtmlPage()
    var itemCount = 0
    val itemCountList = mutableSetOf<String>()

    suspend fun run() {
        var proxy: Proxy? = null
//         proxy = Proxy()
//        proxy.proxyType = Proxy.ProxyType.MANUAL
//        proxy.socksVersion = 5
//        proxy.socksProxy = "176.119.142.108:11040"
//        proxy.socksUsername = "konstantin"
//        proxy.socksPassword = "Handbook232323"
//        proxy.setSslProxy("node-ru-81.astroproxy.com:11039")
//        proxy.setUse
        var webDriver: WebDriver = createWebDriver(isIncognito = false, userAgent = getUserAgentFromFile(), proxy)

        addAdultParam(webDriver)
        allTypeLinks.addAll(modificateLinks(getNeededCategoryLinks(webDriver)))
        //        driver = FirefoxDriver()
        // cpa=1 - покупка только на Маркете
//        var proxyCount = 0
        var pageCount = 0
        while (true) {
            if (allTypeLinks.filter { !it.alreadyUsed }.count() <= 0) {
                break
            }
            try {
                //Переключаемся на следующий прокси
//                if (proxyCount >= proxyList.count() - 1) {
//                    proxyCount = 0
//                } else {
//                    proxyCount++
//                }

                val yandexWebEntity = allTypeLinks.filter { !it.alreadyUsed }.sortedBy { it.link }[0]

                webDriver.get(yandexWebEntity.link)
                pageCount++
                val pageSource = webDriver.pageSource
                if (parseHtmlPage.isHaveCaptcha(pageSource)) {
                    logger.info("На странице найдена КАПТЧА...1")
                    waitForCaptchaWasDisappiar(webDriver)
                }
                logger.info(yandexWebEntity.link)
                val items = getItemsFromSourcePage(pageSource, 5, "Санкт-Петербург")
                itemCount += items.count()
                logger.info(
                    "Всего $itemCount товаров. Осталось необработанных ссылок ${
                        allTypeLinks.filter { !it.alreadyUsed }.count()
                    }."
                )
                if (items.count() > 0) {
                    sendItemsToServer(items)
                    //Добавляем ссылку в коллекцию, если есть следующая страница
                    addNewPageLinkToCollectionIfNeed(pageSource, yandexWebEntity)
                    itemCountList.addAll(items.map { it.productId })
                } else {
                    try {
                        findAllMoreButtonsAndClickIt(webDriver, pageSource)
                    } catch (e: Exception) {
                        logger.info("Do not have links on this page..")
                    }
                    val tempLinks = modificateLinks(getAllCategoryLinksFromPage(webDriver.pageSource))
                    var we1 = tempLinks.map { it.link }.toSet()
                    val we2 = allTypeLinks.map { it.link }.toSet()
                    we1 = we1 - we2
                    allTypeLinks.addAll(we1.map { YandexWebEntity(it, false) })
                }
                //Присваиваем ссылке true, так как ее уже использовали.
                markAsUsed(yandexWebEntity)
                logger.info("=============================Уникальных товаров ${itemCountList.count()}======обработано $pageCount страниц.===================")
            } catch (e: Exception) {
                try {
                    webDriver.quit()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
//                //Если ошибка, то есть возможность пересоздать drive для небольшого ускорения
                webDriver = createWebDriver(isIncognito = false, userAgent = getUserAgentFromFile(), proxy)
                addAdultParam(webDriver)
                e.printStackTrace()
                logger.info("Создадим новый WebDriver")
            }
        }
        webDriver.quit()
    }

    private fun addNewPageLinkToCollectionIfNeed(pageSource: String, yandexWebEntity: YandexWebEntity) {
        val srt = parseHtmlPage.checkNewPage(pageSource, yandexWebEntity)
        if (srt.isNotEmpty()) {
            //Если есть кнопка 'далее', то делаем ссылку на следующую страницу и если ее нет в коллекци - помещаем в коллекцию.
            if (allTypeLinks.filter { it.link == srt }
                    .count() <= 0) {
                allTypeLinks.add(YandexWebEntity(srt))
            }
        }
    }

    private fun markAsUsed(yandexWebEntity: YandexWebEntity) {
        allTypeLinks.remove(yandexWebEntity)
        yandexWebEntity.alreadyUsed = true
        allTypeLinks.add(yandexWebEntity)
    }

    //Добавляем параметры в ссылки, чтобы получить больше товаров с яндекса: сортируем по возрастанию и убыванию цены, по популярности, по размеру скидки
    private fun modificateLinks(neededCategoryLinks: Set<String>): MutableSet<YandexWebEntity> {
        val allTypeLinks = mutableSetOf<YandexWebEntity>()
        val allLinks = try {
            //cpa=1 - покупка только на Маркете, &fesh=431782 - показывать первым ЯМ
            neededCategoryLinks
                .filter { it.substringAfterLast("/").replace("\\d".toRegex(), "").isNotEmpty() }
                .map { YandexWebEntity(link = "$it&cpa=1&onstock=0", alreadyUsed = false) }
                .toMutableSet()
//                "file" -> {
//                    getProxyListFromFile("linksToCategory.txt")
//                        .map { YandexWebEntity(link = "$it&cpa=1&onstock=0", alreadyUsed = false) }
//                        .toMutableSet()
//                }
        } catch (e: Exception) {
            e.printStackTrace()
            mutableSetOf()
        }
        val sortingOptionListForLinks = listOf("&how=aprice", "&how=dprice", "&how=rorp"/*, "&how=discount_p"*/)
        sortingOptionListForLinks.forEach { op ->
            allTypeLinks.addAll(allLinks.map { YandexWebEntity("${it.link}$op") })
        }

        return (allTypeLinks + allLinks) as MutableSet<YandexWebEntity>
//        allTypeLinks = allLinks
    }

    //перед созданием нового экземпляра закройте старый.
    private fun createWebDriver(isIncognito: Boolean, userAgent: String = "", proxy: Proxy?): WebDriver {
        var userProfile = "myChromeProfile"

        System.setProperty("webdriver.chrome.driver", "utils/chrlauncher-win64-stable-ungoogled/bin/chromedriver.exe")
        val chromeOptions = ChromeOptions()
        chromeOptions.addExtensions(File("utils/extensions/extension_0_2_7_0.crx"))
        chromeOptions.setBinary("utils/chrlauncher-win64-stable-ungoogled/bin/chrome.exe")
        if (proxy != null) {
            chromeOptions.setProxy(proxy)
        }
        chromeOptions.addArguments("user-data-dir=$userProfile")

        //        chromeOptions.addArguments("--disable-gpu"/*, "--blink-settings=imagesEnabled=false"*/)
//        chromeOptions.setHeadless(true)
//        val prefs: HashMap<String, Int> = HashMap<String, Int>()
//        prefs["profile.managed_default_content_settings.images"] = 2//не показывать картинки в браузере
//        chromeOptions.setExperimentalOption("prefs", prefs)

//        var proxy = Proxy()
//        proxy.socksVersion = 4
//        proxy.socksProxy = "173.214.244.70:1088"
//        proxy.socksUsername = "s2916"
//        proxy.socksPassword = "Pezoh0oo"
//        proxy.proxyType = Proxy.ProxyType.AUTODETECT
//        var proxy: Proxy? = null
//         proxy = Proxy()
//        proxy.proxyType = Proxy.ProxyType.MANUAL
//        proxy.httpProxy = "173.214.244.70:31284"
////        proxy.socksVersion = 5
////        proxy.socksProxy = "173.214.244.70:1088"
//        proxy.socksUsername = "s2916"
//        proxy.socksPassword = "Pezoh0oo"
//        chromeOptions.setCapability("proxy", proxy)
//        chromeOptions.setProxy(proxy)
//        chromeOptions.addArguments("--proxy-server=http://s2916:Pezoh0oo@173.214.244.70:31284")
//        chromeOptions.addArguments("chrome.switches", "--proxy-server=http://s2916:Pezoh0oo@173.214.244.70:31284")
        if (isIncognito) {
            chromeOptions.addArguments("--incognito")
        }
        if (userAgent.isNotEmpty()) {
            chromeOptions.addArguments("user-agent=\"$userAgent\"")
        }
        val driver: WebDriver = ChromeDriver(chromeOptions)
        //        val driver = FirefoxDriver()
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS)
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS)
//        driver.manage().deleteAllCookies()
        return driver
    }

    fun addAdultParam(driver: WebDriver) {
        driver.get("https://market.yandex.ru/catalog--vibratory/18019241/list")
        if (parseHtmlPage.isHaveCaptcha(driver.pageSource)) {
            logger.info("На странице найдена КАПТЧА...adult")
            waitForCaptchaWasDisappiar(driver)
        }
        try {
            //Жмём на кнопку 'Нам есть 18 лет.'
            val dynamicElement = WebDriverWait(driver, 20)
            val moreButtons =
                dynamicElement.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@data-apiary-widget-id=\"/content/adultWarning\"]//button[@type='button'][1]")))
            moreButtons.forEach {
                try {
                    Actions(driver).moveToElement(it).build().perform()
                    it.click()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
//        driver.findElement(By.xpath("//div[@data-apiary-widget-id=\"/content/adultWarning\"]//button[@type='button'][1]")).click()
    }

    fun getNeededCategoryLinks(driver: WebDriver): Set<String> {
        driver.get("https://market.yandex.ru")
        driver.findElement(By.xpath("//div[@data-apiary-widget-id=\"/header/catalogEntrypoint\"]/button[@type='button']"))
            .click()
        val links = driver
            .findElements(By.xpath("//div[@role=\"tablist\"]/div[@data-zone-name=\"category-link\"]/button[@role=\"tab\"]/a[contains(@href, 'catalog')]"))
            .map { it.getAttribute("href") }
        val neededCategoryLinks = mutableSetOf<String>()

        for (url in links) {
            driver.get(url)
            if (parseHtmlPage.isHaveCaptcha(driver.pageSource)) {
                logger.info("На странице найдена КАПТЧА...2")
                waitForCaptchaWasDisappiar(driver)
            }
            try {
                findAllMoreButtonsAndClickIt(driver, driver.pageSource)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            neededCategoryLinks.addAll(getAllCategoryLinksFromPage(driver.pageSource))
            logger.info("В коллекции ${neededCategoryLinks.size} ссылок.")
        }
        return neededCategoryLinks
    }

    //получаем все ссылки на категории со страницы из меню слева
    private fun getAllCategoryLinksFromPage(pageSource: String): MutableSet<String> {
        val neededCategoryLinks = mutableSetOf<String>()
        val doc: JXDocument = JXDocument.create(pageSource)
        val xpath = "//div[@data-zone-data][@data-zone-name=\"link\"]/a[contains(@href, 'catalog')]"
        val nodes = doc.selN(xpath)
        for ((index, it) in nodes.withIndex()) {
            neededCategoryLinks.add("https://market.yandex.ru${it.asElement().attr("href")}")
            //                logger.info("https://market.yandex.ru${it.asElement().attr("href")}")
        }
        return neededCategoryLinks
    }

    //Ищем в боковом меню слева все кновки 'ещё' и кликаем по ним, чтобы раскрыть все пункты меню
    private fun findAllMoreButtonsAndClickIt(driver: WebDriver, pageSource: String) {
        val doc: JXDocument = JXDocument.create(pageSource)
        val xpath = "//div[@data-zone-data='{\"params\":{\"type\":\"more_button\",\"open\":true}}']/span"
        val nodes = doc.selN(xpath)

        val action = Actions(driver)
        val dynamicElement = WebDriverWait(driver, 10)
        val moreButtons =
            dynamicElement.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@data-zone-data='{\"params\":{\"type\":\"more_button\",\"open\":true}}']/span")))
        for (it in moreButtons) {
            try {
                action.moveToElement(it).build().perform()
                it.click()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun waitForCaptchaWasDisappiar(driver: WebDriver) {
        var i = 1
        while (true) {
            try {
                val webDriverWait = WebDriverWait(driver, 600, 10000)
                webDriverWait.until(ExpectedConditions.urlMatches("https://market.yandex.ru/(?!showcaptcha)"))
                break
            } catch (e: Exception) {
//                e.printStackTrace()
                logger.info("The captcha still here...")
                logger.info("Итерация номер $i")
                i++
            } finally {
                driver.navigate().refresh()
            }
        }
        logger.info("Go forward.")
    }

    fun getItemsFromSourcePage(sourcePage: String, shopId: Long, cityName: String): MutableList<Item> {
        val items = mutableListOf<Item>()
        val elems = Jsoup.parse(sourcePage).select("article[data-zone-data]")
        for (it in elems) {
            try {
                val newItem = Item()
                newItem.price = jsonParser.parseToJsonElement(it.attr("data-zone-data")).jsonObject["price"].toString()
                    .replace("\\D".toRegex(), "").replace("\\D".toRegex(), "").toInt().times(100)
//                newItem.productId = it.select("h3 > a").attr("href").substringBefore("?").substringAfterLast("/")
                newItem.productId = jsonParser.parseToJsonElement(it.attr("data-zone-data")).jsonObject["id"].toString()
                    .replace("\"", "")
                newItem.productName = it.select("h3").text()
                newItem.shopId = shopId
                newItem.cityName = cityName
                newItem.linkToItem = it.select("h3 > a").attr("href").substringBefore("?")
                items.add(newItem)
            } catch (e: Exception) {
//                e.printStackTrace()
            }
        }
        logger.info("Добавили ${elems.count()} элементов.")
        return items
    }

    fun getProxyListFromFile(filePath: String): List<String> {
        return File(filePath).readLines().toSet().toList()
    }

    fun sendItemsToServer(items: MutableList<Item>) {
        GlobalScope.launch(Dispatchers.Unconfined) {
            val klaxon = Klaxon()
            val responseStatus = SendRequest().sendJsonPost(klaxon.toJsonString(items))
            when (responseStatus) {
                "" -> logger.info("Response status is 200. All right!!!")
                else -> { // Note the block
                    logger.info(responseStatus)
                }
            }
        }
    }

    fun getUserAgentFromFile(): String {
        return File("useragentlist.txt").readLines().shuffled().first()
    }
}

class NoLinksException(s: String) : Exception()