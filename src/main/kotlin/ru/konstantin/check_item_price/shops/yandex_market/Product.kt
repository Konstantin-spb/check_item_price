package ru.konstantin.check_item_price.shops.yandex_market


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Product(
    @SerialName("benefit")
    var benefit: Benefit? = null,
    @SerialName("bundleSettings")
    var bundleSettings: BundleSettings? = null,
    @SerialName("categoryIds")
    var categoryIds: List<Int?>? = null,
    @SerialName("classifierMagicId")
    var classifierMagicId: String? = null,
    @SerialName("delivery")
    var delivery: Delivery? = null,
    @SerialName("description")
    var description: String? = null,
    @SerialName("discount")
    var discount: Discount? = null,
    @SerialName("entity")
    var entity: String? = null,
    @SerialName("feed")
    var feed: Feed? = null,
//    @SerialName("filters")
    var filters: List<Filter?>? = null,
    @SerialName("id")
    var id: String? = null,
    @SerialName("isAdult")
    var isAdult: Boolean? = null,
    @SerialName("isAlco")
    var isAlco: Boolean? = null,
    @SerialName("isCutPrice")
    var isCutPrice: Boolean? = null,
    @SerialName("isDefault")
    var isDefault: Boolean? = null,
    @SerialName("isRecommendedByVendor")
    var isRecommendedByVendor: Boolean? = null,
    @SerialName("isSMB")
    var isSMB: Boolean? = null,
    @SerialName("manufacturer")
    var manufacturer: Manufacturer? = null,
    @SerialName("meta")
    var meta: Meta? = null,
    @SerialName("navnodeIds")
    var navnodeIds: List<Int?>? = null,
    @SerialName("offerColor")
    var offerColor: String? = null,
    @SerialName("outletIds")
    var outletIds: List<String?>? = null,
    @SerialName("outletsCount")
    var outletsCount: Int? = null,
    @SerialName("payments")
    var payments: Payments? = null,
    @SerialName("pictures")
    var pictures: List<Picture?>? = null,
    @SerialName("price")
    var price: Price? = null,
    @SerialName("prices")
    var prices: Prices? = null,
    @SerialName("productId")
    var productId: Int? = null,
    @SerialName("promoCodeEnabled")
    var promoCodeEnabled: Boolean? = null,
    @SerialName("returnPolicy")
    var returnPolicy: String? = null,
    @SerialName("seller")
    var seller: Seller? = null,
    @SerialName("shopId")
    var shopId: Int? = null,
    @SerialName("slug")
    var slug: String? = null,
    @SerialName("titles")
    var titles: Titles? = null,
    @SerialName("vendorId")
    var vendorId: Int? = null,
    @SerialName("wareId")
    var wareId: String?
) {
    @Serializable
    data class Benefit(
        @SerialName("description")
        var description: String? = null,
        @SerialName("isPrimary")
        var isPrimary: Boolean? = null,
        @SerialName("nestedTypes")
        var nestedTypes: List<String?>? = null,
        @SerialName("type")
        var type: String?
    )

    @Serializable
    data class BundleSettings(
        @SerialName("quantityLimit")
        var quantityLimit: QuantityLimit?
    ) {
        @Serializable
        data class QuantityLimit(
            @SerialName("minimum")
            var minimum: Int? = null,
            @SerialName("step")
            var step: Int?
        )
    }

    @Serializable
    data class Delivery(
        @SerialName("deliveryPartnerTypes")
        var deliveryPartnerTypes: List<String?>? = null,
        @SerialName("hasLocalStore")
        var hasLocalStore: Boolean? = null,
        @SerialName("hasPickup")
        var hasPickup: Boolean? = null,
        @SerialName("hasPost")
        var hasPost: Boolean? = null,
        @SerialName("inStock")
        var inStock: Boolean? = null,
        @SerialName("isAvailable")
        var isAvailable: Boolean? = null,
        @SerialName("isCountrywide")
        var isCountrywide: Boolean? = null,
        @SerialName("isDownloadable")
        var isDownloadable: Boolean? = null,
        @SerialName("isForcedRegion")
        var isForcedRegion: Boolean? = null,
        @SerialName("isFree")
        var isFree: Boolean? = null,
        @SerialName("isPriorityRegion")
        var isPriorityRegion: Boolean? = null,
        @SerialName("options")
        var options: List<Option?>? = null,
        @SerialName("pickupOptions")
        var pickupOptions: List<PickupOption?>? = null,
        @SerialName("postAvailable")
        var postAvailable: Boolean? = null,
        @SerialName("price")
        var price: Price? = null,
        @SerialName("region")
        var region: Region? = null,
        @SerialName("shopPriorityCountry")
        var shopPriorityCountry: ShopPriorityCountry? = null,
        @SerialName("shopPriorityRegion")
        var shopPriorityRegion: ShopPriorityRegion?
    ) {
        @Serializable
        data class Option(
            @SerialName("dayFrom")
            var dayFrom: Int? = null,
            @SerialName("dayTo")
            var dayTo: Int? = null,
            @SerialName("isDefault")
            var isDefault: Boolean? = null,
            @SerialName("partnerType")
            var partnerType: String? = null,
            @SerialName("price")
            var price: Price? = null,
            @SerialName("region")
            var region: Region? = null,
            @SerialName("serviceId")
            var serviceId: String?
        ) {
            @Serializable
            data class Price(
                @SerialName("currency")
                var currency: String? = null,
                @SerialName("isDeliveryIncluded")
                var isDeliveryIncluded: Boolean? = null,
                @SerialName("isPickupIncluded")
                var isPickupIncluded: Boolean? = null,
                @SerialName("value")
                var value: String?
            )

            @Serializable
            data class Region(
                @SerialName("entity")
                var entity: String? = null,
                @SerialName("id")
                var id: Int? = null,
                @SerialName("lingua")
                var lingua: Lingua? = null,
                @SerialName("name")
                var name: String? = null,
                @SerialName("subtitle")
                var subtitle: String? = null,
                @SerialName("type")
                var type: Int?
            ) {
                @Serializable
                data class Lingua(
                    @SerialName("name")
                    var name: Name?
                ) {
                    @Serializable
                    data class Name(
                        @SerialName("accusative")
                        var accusative: String? = null,
                        @SerialName("genitive")
                        var genitive: String? = null,
                        @SerialName("preposition")
                        var preposition: String? = null,
                        @SerialName("prepositional")
                        var prepositional: String?
                    )
                }
            }
        }

        @Serializable
        data class PickupOption(
            @SerialName("dayFrom")
            var dayFrom: Int? = null,
            @SerialName("dayTo")
            var dayTo: Int? = null,
            @SerialName("groupCount")
            var groupCount: Int? = null,
            @SerialName("orderBefore")
            var orderBefore: Int? = null,
            @SerialName("partnerType")
            var partnerType: String? = null,
            @SerialName("price")
            var price: Price? = null,
            @SerialName("region")
            var region: Region? = null,
            @SerialName("serviceId")
            var serviceId: Int? = null,
            @SerialName("serviceName")
            var serviceName: String? = null,
            @SerialName("tariffId")
            var tariffId: Int?
        ) {
            @Serializable
            data class Price(
                @SerialName("currency")
                var currency: String? = null,
                @SerialName("value")
                var value: String?
            )

            @Serializable
            data class Region(
                @SerialName("entity")
                var entity: String? = null,
                @SerialName("id")
                var id: Int? = null,
                @SerialName("lingua")
                var lingua: Lingua? = null,
                @SerialName("name")
                var name: String? = null,
                @SerialName("subtitle")
                var subtitle: String? = null,
                @SerialName("type")
                var type: Int?
            ) {
                @Serializable
                data class Lingua(
                    @SerialName("name")
                    var name: Name?
                ) {
                    @Serializable
                    data class Name(
                        @SerialName("accusative")
                        var accusative: String? = null,
                        @SerialName("genitive")
                        var genitive: String? = null,
                        @SerialName("preposition")
                        var preposition: String? = null,
                        @SerialName("prepositional")
                        var prepositional: String?
                    )
                }
            }
        }

        @Serializable
        data class Price(
            @SerialName("currency")
            var currency: String? = null,
            @SerialName("isDeliveryIncluded")
            var isDeliveryIncluded: Boolean? = null,
            @SerialName("isPickupIncluded")
            var isPickupIncluded: Boolean? = null,
            @SerialName("value")
            var value: String?
        )

        @Serializable
        data class Region(
            @SerialName("entity")
            var entity: String? = null,
            @SerialName("id")
            var id: Int? = null,
            @SerialName("lingua")
            var lingua: Lingua? = null,
            @SerialName("name")
            var name: String? = null,
            @SerialName("subtitle")
            var subtitle: String? = null,
            @SerialName("type")
            var type: Int?
        ) {
            @Serializable
            data class Lingua(
                @SerialName("name")
                var name: Name?
            ) {
                @Serializable
                data class Name(
                    @SerialName("accusative")
                    var accusative: String? = null,
                    @SerialName("genitive")
                    var genitive: String? = null,
                    @SerialName("preposition")
                    var preposition: String? = null,
                    @SerialName("prepositional")
                    var prepositional: String?
                )
            }
        }

        @Serializable
        data class ShopPriorityCountry(
            @SerialName("entity")
            var entity: String? = null,
            @SerialName("id")
            var id: Int? = null,
            @SerialName("lingua")
            var lingua: Lingua? = null,
            @SerialName("name")
            var name: String? = null,
            @SerialName("type")
            var type: Int?
        ) {
            @Serializable
            data class Lingua(
                @SerialName("name")
                var name: Name?
            ) {
                @Serializable
                data class Name(
                    @SerialName("accusative")
                    var accusative: String? = null,
                    @SerialName("genitive")
                    var genitive: String? = null,
                    @SerialName("preposition")
                    var preposition: String? = null,
                    @SerialName("prepositional")
                    var prepositional: String?
                )
            }
        }

        @Serializable
        data class ShopPriorityRegion(
            @SerialName("entity")
            var entity: String? = null,
            @SerialName("id")
            var id: Int? = null,
            @SerialName("lingua")
            var lingua: Lingua? = null,
            @SerialName("name")
            var name: String? = null,
            @SerialName("subtitle")
            var subtitle: String? = null,
            @SerialName("type")
            var type: Int?
        ) {
            @Serializable
            data class Lingua(
                @SerialName("name")
                var name: Name?
            ) {
                @Serializable
                data class Name(
                    @SerialName("accusative")
                    var accusative: String? = null,
                    @SerialName("genitive")
                    var genitive: String? = null,
                    @SerialName("preposition")
                    var preposition: String? = null,
                    @SerialName("prepositional")
                    var prepositional: String?
                )
            }
        }
    }

    @Serializable
    data class Discount(
        @SerialName("currentPrice")
        var currentPrice: CurrentPrice?
    ) {
        @Serializable
        data class CurrentPrice(
            @SerialName("currency")
            var currency: String? = null,
            @SerialName("value")
            var value: Int?
        )
    }

    @Serializable
    data class Feed(
        @SerialName("id")
        var id: Int? = null,
        @SerialName("offerId")
        var offerId: String?
    )

    @Serializable
    data class Filter(
        @SerialName("id")
        var id: String? = null,
        @SerialName("isGuruLight")
        var isGuruLight: Boolean? = null,
        @SerialName("kind")
        var kind: Int? = null,
        @SerialName("meta")
        var meta: Meta? = null,
        @SerialName("name")
        var name: String? = null,
        @SerialName("noffers")
        var noffers: Int? = null,
        @SerialName("position")
        var position: Int? = null,
        @SerialName("subType")
        var subType: String? = null,
        @SerialName("type")
        var type: String? = null,
        @SerialName("values")
        var values: List<Value?>? = null,
        @SerialName("valuesCount")
        var valuesCount: Int? = null,
        @SerialName("valuesGroups")
        var valuesGroups: List<ValuesGroup?>? = null,
        @SerialName("xslname")
        var xslname: String?
    ) {
        @Serializable
        class Meta(
        )

        @Serializable
        data class Value(
            @SerialName("code")
            var code: String? = null,
            @SerialName("found")
            var found: Int? = null,
            @SerialName("id")
            var id: String? = null,
            @SerialName("initialFound")
            var initialFound: Int? = null,
            @SerialName("value")
            var value: String?
        )

        @Serializable
        data class ValuesGroup(
            @SerialName("type")
            var type: String? = null,
            @SerialName("valuesIds")
            var valuesIds: List<String?>?
        )
    }

    @Serializable
    data class Manufacturer(
        @SerialName("entity")
        var entity: String? = null,
        @SerialName("warranty")
        var warranty: Boolean?
    )

    @Serializable
    class Meta(
    )

    @Serializable
    data class Payments(
        @SerialName("deliveryCard")
        var deliveryCard: Boolean? = null,
        @SerialName("deliveryCash")
        var deliveryCash: Boolean? = null,
        @SerialName("prepaymentCard")
        var prepaymentCard: Boolean? = null,
        @SerialName("prepaymentOther")
        var prepaymentOther: Boolean?
    )

    @Serializable
    data class Picture(
        @SerialName("entity")
        var entity: String? = null,
        @SerialName("original")
        var original: Original? = null,
        @SerialName("signatures")
        var signatures: List<String?>? = null,
        @SerialName("thumbnails")
        var thumbnails: List<Thumbnail?>?
    ) {
        @Serializable
        data class Original(
            @SerialName("containerHeight")
            var containerHeight: Int? = null,
            @SerialName("containerWidth")
            var containerWidth: Int? = null,
            @SerialName("height")
            var height: Int? = null,
            @SerialName("url")
            var url: String? = null,
            @SerialName("width")
            var width: Int?
        )

        @Serializable
        data class Thumbnail(
            @SerialName("containerHeight")
            var containerHeight: Int? = null,
            @SerialName("containerWidth")
            var containerWidth: Int? = null,
            @SerialName("height")
            var height: Int? = null,
            @SerialName("url")
            var url: String? = null,
            @SerialName("width")
            var width: Int?
        )
    }

    @Serializable
    data class Price(
        @SerialName("currency")
        var currency: String? = null,
        @SerialName("value")
        var value: Int?
    )

    @Serializable
    data class Prices(
        @SerialName("currency")
        var currency: String? = null,
        @SerialName("isDeliveryIncluded")
        var isDeliveryIncluded: Boolean? = null,
        @SerialName("isPickupIncluded")
        var isPickupIncluded: Boolean? = null,
        @SerialName("rawValue")
        var rawValue: String? = null,
        @SerialName("value")
        var value: String?
    )

    @Serializable
    data class Seller(
        @SerialName("comment")
        var comment: String? = null,
        @SerialName("currency")
        var currency: String? = null,
        @SerialName("price")
        var price: String? = null,
        @SerialName("sellerToUserExchangeRate")
        var sellerToUserExchangeRate: Int?
    )

    @Serializable
    data class Titles(
        @SerialName("highlighted")
        var highlighted: List<Highlighted?>? = null,
        @SerialName("raw")
        var raw: String?
    ) {
        @Serializable
        data class Highlighted(
            @SerialName("value")
            var value: String?
        )
    }
}