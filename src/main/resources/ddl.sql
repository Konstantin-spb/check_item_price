create table if not exists shops
(
    id serial not null
        constraint shop_pk
            primary key,
    name varchar(50) not null,
    city_id integer
);

alter table shops owner to check_item_price;

create unique index if not exists shop_id_uindex
    on shops (id);

create unique index if not exists shop_name_uindex
    on shops (name);

create table if not exists items
(
    id bigserial not null
        constraint items_pk
            primary key,
    name varchar(255) not null,
    product_id bigint,
    shop_id integer not null
        constraint items_shops_id_fk
            references shops,
    price_id integer not null
);

alter table items owner to check_item_price;

create unique index if not exists items_id_uindex
    on items (id);

create table if not exists prises
(
    id bigserial not null
        constraint prises_pk
            primary key,
    sum integer not null,
    parse_date timestamp not null,
    item_id bigint not null
        constraint prises_items_id_fk
            references items
            on update cascade on delete cascade
);

alter table items_properties owner to check_item_price;

create unique index if not exists prises_id_uindex
    on prises (id);
