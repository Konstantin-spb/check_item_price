import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

var ktorVersion = "1.4.1"

plugins {
    val kotlinVer = "1.4.30"
    //    id("application")
    id("java")
    id("com.github.johnrengelman.shadow") version "5.2.0"
//    id("org.springframework.boot") version "2.2.2.RELEASE"
//    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    kotlin("jvm") version kotlinVer
//    kotlin("plugin.spring") version "1.3.61"
//    kotlin("plugin.jpa") version "1.3.61"
    kotlin("plugin.serialization") version kotlinVer
}
group = "ru.konstantin"
version = "0.0.1"

java.sourceCompatibility = JavaVersion.VERSION_11
val developmentOnly by configurations.creating
configurations {
    runtimeClasspath {
        extendsFrom(developmentOnly)
    }
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}
repositories {
    mavenCentral()
    jcenter()
    maven { url=uri("https://jitpack.io") }
}
dependencies {
//    implementation(fileTree("libs") { include("*.jar") })
    implementation("com.github.kittinunf.fuel:fuel:2.3.1")
//    implementation("io.ktor:ktor-client-cio:$ktorVersion")
//    implementation("com.github.valdio:SerializableCookieJar:1.0.0")
    implementation("com.squareup.okhttp3:okhttp:4.9.0")
    implementation("org.apache.httpcomponents:httpclient:4.5.11")
//    implementation("org.seleniumhq.selenium:selenium-chrome-driver:4.0.0-alpha-6")
    implementation("org.seleniumhq.selenium:selenium-chrome-driver:3.141.59")
//    implementation("org.seleniumhq.selenium:selenium-java:4.0.0-alpha-6")
    implementation("org.seleniumhq.selenium:selenium-java:3.141.59")
//    implementation("org.slf4j:slf4j-api:1.7.30")
    implementation("org.apache.logging.log4j:log4j-core:2.13.0")
//    implementation("net.sourceforge.htmlunit:htmlunit:2.37.0")
    implementation("org.json:json:20200518")
    implementation("cn.wanghaomiao:JsoupXpath:2.4.3")
//    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
//    implementation("io.ktor:ktor-client-apache:1.3.1")
//    developmentOnly("org.springframework.boot:spring-boot-devtools")
//    runtimeOnly("org.postgresql:postgresql")
//    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
//    testImplementation("org.springframework.boot:spring-boot-starter-test") {
//        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
//    }
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.8")
//    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.8")
    implementation("org.jsoup:jsoup:1.12.2")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation("com.beust:klaxon:5.0.1")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.11.1")
    testImplementation("junit:junit:4.12")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.jar {
    manifest {
        attributes(
//                "Implementation-Title" to "Gradle",
//                "Implementation-Version" to archiveVersion,
//                "Main-Class" to "ru.konstantin.check_item_price.CheckItemPriceApplicationKt"
                "Main-Class" to "ru.konstantin.check_item_price.TestHereKt"
        )
    }
}